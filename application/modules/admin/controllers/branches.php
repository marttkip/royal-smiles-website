<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/admin/controllers/admin.php";

class Branches extends admin 
{
	var $branches_path;
	var $branches_location;
	var $document_upload_path;
	var $document_upload_location;
	var $personnel_path;
	var $csv_path;
	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('users_model');
		$this->load->model('branches_model');
		$this->load->model('file_model');
		
		$this->load->library('image_lib');
		
		//path to image directory
		$this->branches_path = realpath(APPPATH . '../assets/logo');
		$this->branches_location = base_url().'assets/logo/';
		
		//path to imports
		$this->csv_path = realpath(APPPATH . '../assets/csv');
	}
    
	/*
	*
	*	Default action is to show all the branches
	*
	*/
	public function index($order = 'branch_name', $order_method = 'ASC') 
	{
		$where = 'branch_id > 0';
		$table = 'branch';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'admin/branches/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->branches_model->get_all_branches($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Branches';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['all_branches'] = $this->branches_model->all_branches();
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('branches/all_branches', $v_data, true);
		
		$this->load->view('templates/general_page', $data);
	}
    
	/*
	*
	*	Add a new branch
	*
	*/
	public function add_branch() 
	{
		$v_data['branch_location'] = 'http://placehold.it/200x200';
		
		$this->session->unset_userdata('branch_error_message');
		
		//upload image if it has been selected
		$response = $this->branches_model->upload_branch_logo($this->branches_path, $this->branches_location);
		if($response)
		{
			$v_data['branch_location'] = $this->branches_location.$this->session->userdata('branch_file_name');
		}
		
		//case of upload error
		else
		{
			$v_data['branch_error'] = $this->session->userdata('branch_error_message');
		}
		
		$branch_error = $this->session->userdata('branch_error_message');
		
		//form validation rules
		$this->form_validation->set_rules('branch_name', 'Branch name', 'required|xss_clean');
		$this->form_validation->set_rules('branch_phone', 'Phone', 'xss_clean');
		$this->form_validation->set_rules('branch_email', 'Email', 'valid_email|xss_clean');
		$this->form_validation->set_rules('branch_working_weekday', 'Weekday working hours', 'xss_clean');
		$this->form_validation->set_rules('branch_working_weekend', 'Weekend working hours', 'xss_clean');
		$this->form_validation->set_rules('branch_address', 'Address', 'xss_clean');
		$this->form_validation->set_rules('branch_city', 'City', 'xss_clean');
		$this->form_validation->set_rules('branch_post_code', 'Post code', 'xss_clean');
		$this->form_validation->set_rules('branch_location', 'Location', 'xss_clean');
		$this->form_validation->set_rules('branch_building', 'Building', 'xss_clean');
		$this->form_validation->set_rules('branch_floor', 'Floor', 'xss_clean');
		$this->form_validation->set_rules('branch_status', 'Status', 'xss_clean');
		$this->form_validation->set_rules('branch_parent', 'Branch parent', 'xss_clean');
		$this->form_validation->set_rules('branch_code', 'Branch code', 'required|is_unique[branch.branch_code]|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if(empty($branch_error))
			{
				if($this->branches_model->add_branch($this->session->userdata('branch_file_name'), $this->session->userdata('branch_thumb_name')))
				{
					$this->session->unset_userdata('branch_file_name');
					$this->session->unset_userdata('branch_thumb_name');
					$this->session->unset_userdata('branch_error_message');
					$this->session->set_userdata('success_message', 'Branch added successfully');
					redirect('administration/branches');
				}
				
				else
				{
					$this->session->set_userdata('error_message', 'Could not add branch. Please try again');
				}
			}
		}
		
		//open the add new branch
		
		$v_data['all_branches'] = $this->branches_model->all_branches();
		$data['title'] = 'Add branch';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('branches/add_branch', $v_data, true);
		$this->load->view('templates/general_page', $data);
	}
    
	/*
	*
	*	Edit an existing branch
	*	@param int $branch_id
	*
	*/
	public function edit_branch($branch_id) 
	{
		//select the branch from the database
		$query = $this->branches_model->get_branch($branch_id);
		$slide_row = $query->row();
		$v_data['query'] = $query;
		$v_data['branch_img_location'] = $this->branches_location.$slide_row->branch_image_name;
		//
		$this->session->unset_userdata('branch_error_message');
		
		//upload image if it has been selected
		$response = $this->branches_model->upload_branch_logo($this->branches_path, $this->branches_location);
		if($response)
		{
			$v_data['branch_img_location'] = $this->branches_location.$this->session->userdata('branch_file_name');
		}
		
		//case of upload error
		else
		{
			$v_data['branch_error'] = $this->session->userdata('branch_error_message');
		}
		
		$branch_error = $this->session->userdata('branch_error_message');
		
		//form validation rules
		$this->form_validation->set_rules('branch_name', 'Branch name', 'required|xss_clean');
		$this->form_validation->set_rules('branch_phone', 'Phone', 'xss_clean');
		$this->form_validation->set_rules('branch_email', 'Email', 'valid_email|xss_clean');
		$this->form_validation->set_rules('branch_working_weekday', 'Weekday working hours', 'xss_clean');
		$this->form_validation->set_rules('branch_working_weekend', 'Weekend working hours', 'xss_clean');
		$this->form_validation->set_rules('branch_address', 'Address', 'xss_clean');
		$this->form_validation->set_rules('branch_city', 'City', 'xss_clean');
		$this->form_validation->set_rules('branch_post_code', 'Post code', 'xss_clean');
		$this->form_validation->set_rules('branch_location', 'Location', 'xss_clean');
		$this->form_validation->set_rules('branch_building', 'Building', 'xss_clean');
		$this->form_validation->set_rules('branch_floor', 'Floor', 'xss_clean');
		$this->form_validation->set_rules('branch_status', 'Status', 'xss_clean');
		$this->form_validation->set_rules('branch_parent', 'Branch parent', 'xss_clean');
		$this->form_validation->set_rules('branch_code', 'Branch code', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if(empty($branch_error))
			{
				$image = $this->session->userdata('branch_file_name');
				$thumb = $this->session->userdata('branch_thumb_name');
				
				if($image == FALSE)
				{
					$image = $slide_row->branch_image_name;
					$thumb = $slide_row->branch_thumb_name;
				}
				if($this->branches_model->update_branch($image, $thumb, $branch_id))
				{
					$this->session->unset_userdata('branch_file_name');
					$this->session->unset_userdata('branch_thumb_name');
					$this->session->unset_userdata('branch_error_message');
					$this->session->set_userdata('success_message', 'Branch updated successfully');
					redirect('administration/branches');
				}
				
				else
				{
					$this->session->set_userdata('error_message', 'Could not update branch. Please try again');
				}
			}
		}
		
		//open the add new branch
		$v_data['all_branches'] = $this->branches_model->all_branches();
		$data['title'] = 'Edit branch';
		$v_data['title'] = $data['title'];
		
		if ($query->num_rows() > 0)
		{
			$v_data['branch'] = $query->row();
			
			$data['content'] = $this->load->view('branches/edit_branch', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'Branch does not exist';
		}
		
		$this->load->view('templates/general_page', $data);
	}
    
	/*
	*
	*	Delete an existing branch
	*	@param int $branch_id
	*
	*/
	public function delete_branch($branch_id)
	{
		//delete branch image
		$query = $this->branches_model->get_branch($branch_id);
		
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
			$image = $result[0]->branch_image_name;
			$this->load->model('file_model');
			//delete image
			// echo $this->branches_path."/images/".$image;
			// $this->file_model->delete_file($slideshow_path."\\".$image_name, $this->slideshow_path);
			$this->file_model->delete_file($this->branches_path."/images/".$image, $this->slideshow_path);
			//delete thumbnail
			$this->file_model->delete_file($this->branches_path."/thumbs/".$image, $this->slideshow_path);
		}
		$this->branches_model->delete_branch($branch_id);
		$this->session->set_userdata('success_message', 'Branch has been deleted');
		redirect('admin/branches');
	}
    
	/*
	*
	*	Activate an existing branch
	*	@param int $branch_id
	*
	*/
	public function activate_branch($branch_id)
	{
		$this->branches_model->activate_branch($branch_id);
		$this->session->set_userdata('success_message', 'Branch activated successfully');
		redirect('admin/branches');
	}
    
	/*
	*
	*	Deactivate an existing branch
	*	@param int $branch_id
	*
	*/
	public function deactivate_branch($branch_id)
	{
		$this->branches_model->deactivate_branch($branch_id);
		$this->session->set_userdata('success_message', 'Branch disabled successfully');
		redirect('admin/branches');
	}
	
	//import branches
	function import_branches()
	{
		$v_data['title'] = $data['title'] = $this->site_model->display_page_title();
		
		$data['content'] = $this->load->view('import/import_branches', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	function import_branches_template()
	{
		//export products template in excel 
		$this->branches_model->import_branches_template();
	}
	//do the branch import
	function do_branches_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import branch from excel 
				$response = $this->branches_model->import_csv_branch($this->csv_path);
				
				if($response == FALSE)
				{
					$v_data['import_response_error'] = 'Something went wrong. Please try again.';
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		
		$v_data['title'] = $data['title'] = $this->site_model->display_page_title();
		
		$data['content'] = $this->load->view('import/import_branches', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	
	
}
?>