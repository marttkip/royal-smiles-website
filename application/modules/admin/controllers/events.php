<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/admin/controllers/admin.php";

class Events extends admin {
	var $event_path;
	var $event_location;
	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('users_model');
		$this->load->model('event_model');
		$this->load->model('file_model');
		
		$this->load->library('image_lib');
		
		//path to image directory
		$this->event_path = realpath(APPPATH . '../assets/event');
		$this->event_location = base_url().'assets/event/';
	}
    
	/*
	*
	*	Default action is to show all the registered event
	*
	*/
	public function index() 
	{
		$where = 'event.event_type_id = event_type.event_type_id';
		$table = 'event, event_type';
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'content/events';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->event_model->get_all_events($table, $where, $config["per_page"], $page);
		
		if ($query->num_rows() > 0)
		{
			$v_data['query'] = $query;
			$v_data['page'] = $page;
			$v_data['event_location'] = $this->event_location;
			$data['content'] = $this->load->view('event/all_events', $v_data, true);
		}
		
		else
		{
			$data['content'] = '<a href="'.site_url().'administration/add-event" class="btn btn-success pull-right">Add Event</a>There are no events';
		}
		$data['title'] = 'All Events';
		
		$this->load->view('templates/general_page', $data);
	}
	
	function add_event()
	{
		$v_data['event_location'] = 'http://placehold.it/500x500';
		
		$this->session->unset_userdata('event_error_message');
		
		//upload image if it has been selected
		$response = $this->event_model->upload_event_image($this->event_path);
		if($response)
		{
			$v_data['event_location'] = $this->event_location.$this->session->userdata('event_file_name');
		}
		
		//case of upload error
		else
		{
			$v_data['event_error'] = $this->session->userdata('event_error_message');
		}
		
		$event_error = $this->session->userdata('event_error_message');
		
		$this->form_validation->set_rules('event_type_id', 'Event Type', 'required|xss_clean');
		$this->form_validation->set_rules('event_name', 'Title', 'required|xss_clean');
		$this->form_validation->set_rules('event_description', 'Description', 'trim|xss_clean');
		$this->form_validation->set_rules('event_venue', 'Venue', 'xss_clean');
		$this->form_validation->set_rules('event_start_time', 'Start Time', 'xss_clean');
		$this->form_validation->set_rules('event_end_time', 'End Time', 'trim|xss_clean');
		$this->form_validation->set_rules('event_location', 'Location', 'xss_clean');
		$this->form_validation->set_rules('event_admission', 'Member Cost', 'trim|xss_clean');
		$this->form_validation->set_rules('event_latitude', 'Latitude', 'trim|xss_clean');
		$this->form_validation->set_rules('event_longitude', 'Longitude', 'xss_clean');
		$this->form_validation->set_rules('event_timetable', 'Timetable', 'xss_clean');
		$this->form_validation->set_rules('event_time', 'Event Time', 'xss_clean');
		$this->form_validation->set_rules('event_admission_non_member', 'Non Member Cost', 'xss_clean');

		if ($this->form_validation->run())
		{	
			if(empty($event_error))
			{
				$data2 = array(
					'event_type_id'=>$this->input->post("event_type_id"),
					'event_name'=>$this->input->post("event_name"),
					'event_description'=>$this->input->post("event_description"),
					'event_venue'=>$this->input->post("event_venue"),
					'event_start_time'=>$this->input->post("event_start_time"),
					'event_end_time'=>$this->input->post("event_end_time"),
					'event_location'=>$this->input->post("event_location"),
					'event_admission'=>$this->input->post("event_admission"),
					'event_latitude'=>$this->input->post("event_latitude"),
					'event_longitude'=>$this->input->post("event_longitude"),
					'event_timetable'=>$this->input->post("event_timetable"),
					'event_time'=>$this->input->post("event_time"),
					'event_admission_non_member'=>$this->input->post("event_admission_non_member"),
					'event_status'=>1,
					'event_image_name'=>$this->session->userdata('event_file_name'),
					'event_web_name' => $this->users_model->create_web_name($this->input->post("event_name")),
					'created_by' => $this->session->userdata('user_id'),
					'created' => date('Y-m-d H:i:s'),
					'modified_by' => $this->session->userdata('user_id')
				);
				
				$table = "event";
				$this->db->insert($table, $data2);
				$this->session->unset_userdata('event_file_name');
				$this->session->unset_userdata('event_thumb_name');
				$this->session->unset_userdata('event_error_message');
				$this->session->set_userdata('success_message', 'Event has been added');
				
				redirect('content/events');
			}
		}
		
		$event = $this->session->userdata('event_file_name');
		
		if(!empty($event))
		{
			$v_data['event_location'] = $this->event_location.$this->session->userdata('event_file_name');
		}
		$v_data['error'] = $event_error;
		$v_data['event_types'] = $this->db->get('event_type');
		
		$data['content'] = $this->load->view("event/add_event", $v_data, TRUE);
		$data['title'] = 'Add Event';
		
		$this->load->view('templates/general_page', $data);
	}
	
	public function edit_event($event_id, $page)
	{
		//get event data
		$table = "event";
		$where = "event_id = ".$event_id;
		
		$this->db->where($where);
		$events_query = $this->db->get($table);
		$event_row = $events_query->row();
		$v_data['event_row'] = $event_row;
		$v_data['event_location'] = $this->event_location.$event_row->event_image_name;
		
		$this->session->unset_userdata('event_error_message');
		
		//upload image if it has been selected
		$response = $this->event_model->upload_event_image($this->event_path, $edit = $event_row->event_image_name);
		if($response)
		{
			$v_data['event_location'] = $this->event_location.$this->session->userdata('event_file_name');
		}
		
		//case of upload error
		else
		{
			$v_data['event_error'] = $this->session->userdata('event_error_message');
		}
		
		$event_error = $this->session->userdata('event_error_message');
		
		$this->form_validation->set_rules('event_type_id', 'Event Type', 'required|xss_clean');
		$this->form_validation->set_rules('event_name', 'Title', 'required|xss_clean');
		$this->form_validation->set_rules('event_description', 'Description', 'trim|xss_clean');
		$this->form_validation->set_rules('event_venue', 'Venue', 'xss_clean');
		$this->form_validation->set_rules('event_start_time', 'Start Time', 'xss_clean');
		$this->form_validation->set_rules('event_end_time', 'End Time', 'trim|xss_clean');
		$this->form_validation->set_rules('event_location', 'Location', 'xss_clean');
		$this->form_validation->set_rules('event_admission', 'Member Cost', 'trim|xss_clean');
		$this->form_validation->set_rules('event_latitude', 'Latitude', 'trim|xss_clean');
		$this->form_validation->set_rules('event_longitude', 'Longitude', 'xss_clean');
		$this->form_validation->set_rules('event_timetable', 'Timetable', 'xss_clean');
		$this->form_validation->set_rules('event_time', 'Event Time', 'xss_clean');
		$this->form_validation->set_rules('event_admission_non_member', 'Non Member Cost', 'xss_clean');

		if ($this->form_validation->run())
		{	
			if(empty($event_error))
			{
				$event = $this->session->userdata('event_file_name');
				
				if($event == FALSE)
				{
					$event = $event_row->event_image_name;
				}
				$data2 = array(
					'event_type_id'=>$this->input->post("event_type_id"),
					'event_name'=>$this->input->post("event_name"),
					'event_description'=>$this->input->post("event_description"),
					'event_venue'=>$this->input->post("event_venue"),
					'event_start_time'=>$this->input->post("event_start_time"),
					'event_end_time'=>$this->input->post("event_end_time"),
					'event_location'=>$this->input->post("event_location"),
					'event_admission'=>$this->input->post("event_admission"),
					'event_latitude'=>$this->input->post("event_latitude"),
					'event_longitude'=>$this->input->post("event_longitude"),
					'event_timetable'=>$this->input->post("event_timetable"),
					'event_time'=>$this->input->post("event_time"),
					'event_admission_non_member'=>$this->input->post("event_admission_non_member"),
					'event_image_name'=>$event,
					'event_web_name' => $this->users_model->create_web_name($this->input->post("event_name")),
					'modified_by' => $this->session->userdata('user_id')
				);
				
				$table = "event";
				$this->db->where('event_id', $event_id);
				$this->db->update($table, $data2);
				$this->session->unset_userdata('event_file_name');
				$this->session->unset_userdata('event_thumb_name');
				$this->session->unset_userdata('event_error_message');
				$this->session->set_userdata('success_message', 'Event has been edited');
				
				redirect('content/events/'.$page);
			}
		}
		
		$event = $this->session->userdata('event_file_name');
		
		if(!empty($event))
		{
			$v_data['event_location'] = $this->event_location.$this->session->userdata('event_file_name');
		}
		$v_data['error'] = $event_error;
		$v_data['page'] = $page;
		$v_data['event_id'] = $event_id;
		$v_data['event_types'] = $this->db->get('event_type');
		$v_data['event_location_parnters'] = $this->event_location;
		// $v_data['event_partners'] = $this->event_model->get_event_partners($event_id);
		
		$data['content'] = $this->load->view("event/edit_event", $v_data, TRUE);
		$data['title'] = 'Edit Event';
		
		$this->load->view('templates/general_page', $data);
	}
    
	/*
	*
	*	Delete an existing event
	*	@param int $event_id
	*
	*/
	function delete_event($event_id, $page)
	{
		//get event data
		$table = "event";
		$where = "event_id = ".$event_id;
		
		$this->db->where($where);
		$events_query = $this->db->get($table);
		$event_row = $events_query->row();
		$event_path = $this->event_path;
		
		$image_name = $event_row->event_image_name;
		
		//delete any other uploaded image
		$this->file_model->delete_file($event_path."\\".$image_name);
		
		//delete any other uploaded thumbnail
		$this->file_model->delete_file($event_path."\\thumbnail_".$image_name);
		
		if($this->event_model->delete_event($event_id))
		{
			$this->session->set_userdata('success_message', 'Event has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Event could not be deleted');
		}
		redirect('content/events/'.$page);
	}
    
	/*
	*
	*	Activate an existing event
	*	@param int $event_id
	*
	*/
	public function activate_event($event_id, $page)
	{
		if($this->event_model->activate_event($event_id))
		{
			$this->session->set_userdata('success_message', 'Event has been activated');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Event could not be activated');
		}
		redirect('content/events/'.$page);
	}
    
	/*
	*
	*	Deactivate an existing event
	*	@param int $event_id
	*
	*/
	public function deactivate_event($event_id, $page)
	{
		if($this->event_model->deactivate_event($event_id))
		{
			$this->session->set_userdata('success_message', 'Event has been disabled');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Event could not be disabled');
		}
		redirect('content/events/'.$page);
	}
	
	public function update_event_web_names()
	{
		$query = $this->db->get('event');
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $res)
			{
				$event_id = $res->event_id;
				$event_name = $res->event_name;
				$event_start_time = ''.$res->event_start_time.'';
				$event_web_name = $this->users_model->create_web_name($event_name.''.$event_start_time);
				$data2 = array(
					'event_web_name' => $event_web_name,
				);
				
				$table = "event";
				$this->db->where('event_id', $event_id);
				if($this->db->update($table, $data2))
				{
					echo $event_name.' - '.$event_web_name.'<br/>';
				}
			}
		}
	}
	
	public function add_event_partner($event_id, $page = 0)
	{
		$this->session->unset_userdata('event_error_message');
		
		//upload image if it has been selected
		$response = $this->event_model->upload_event_partner_image($this->event_path);
		if($response)
		{
			$v_data['event_location'] = $this->event_location.$this->session->userdata('event_file_name');
		}
		
		//case of upload error
		else
		{
			$event_error = $this->session->userdata('event_error_message');
			$this->session->set_userdata('error_message', $event_error);
			$this->session->unset_userdata('event_error_message');
		}
		
		$event_error = $this->session->userdata('event_error_message');
		
		$this->form_validation->set_rules('event_partners_name', 'Name', 'required|xss_clean');
		$this->form_validation->set_rules('event_partners_location', 'Website URL', 'trim|xss_clean');
		$this->form_validation->set_rules('event_partners_type', 'Type', 'trim|xss_clean');
		$this->form_validation->set_rules('event_partners_description', 'Description', 'xss_clean');

		if ($this->form_validation->run())
		{	
			if(empty($event_error))
			{
				$event = $this->session->userdata('event_partner_file_name');
				
				if($event == FALSE)
				{
					$event = $event_row->event_image_name;
				}
				$data2 = array(
					'event_partners_name'=>$this->input->post("event_partners_name"),
					'event_partners_link'=>$this->input->post("event_partners_location"),
					'event_partners_type'=>$this->input->post("event_partners_type"),
					'event_partners_description'=>$this->input->post("event_partners_description"),
					'event_partners_image_name'=>$event,
					'event_id'=>$event_id,
				);
				
				$table = "event_partners";
				$this->db->where('event_id', $event_id);
				if($this->db->insert($table, $data2))
				{
					$this->session->unset_userdata('event_partner_file_name');
					$this->session->unset_userdata('event_partner_thumb_name');
					$this->session->unset_userdata('event_error_message');
					$this->session->set_userdata('success_message', 'Event partner has been added');
				}
				
				else
				{
					$this->session->set_userdata('error_message', 'Event partner could not be added');
				}
			}
		}
		
		redirect('administration/edit-event/'.$event_id.'/'.$page);
	}
    
	/*
	*
	*	Delete an existing event
	*	@param int $event_id
	*
	*/
	function delete_event_partners($event_id, $event_partners_id, $page)
	{
		//get event data
		$table = "event_partners";
		$where = "event_partners_id = ".$event_partners_id;
		
		$this->db->where($where);
		$events_query = $this->db->get($table);
		
		if($events_query->num_rows() > 0)
		{
			$event_row = $events_query->row();
			$event_path = $this->event_path;
			
			$image_name = $event_row->event_partners_image_name;
			
			//delete any other uploaded image
			$this->file_model->delete_file($event_path."\\".$image_name, $event_path);
			
			//delete any other uploaded thumbnail
			$this->file_model->delete_file($event_path."\\thumbnail_".$image_name, $event_path);
		}
		$this->db->where('event_partners_id', $event_partners_id);
		if($this->db->delete($table))
		{
			$this->session->set_userdata('success_message', 'Partner has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Partner could not be deleted');
		}
		redirect('administration/edit-event/'.$event_id.'/'.$page);
	}
	
	
	public function edit_event_partners($event_id, $event_partners_id, $page)
	{
		//get event data
		$table = "event_partners";
		$where = "event_partners_id = ".$event_partners_id;
		
		$this->db->where($where);
		$events_query = $this->db->get($table);
		$event_row = $events_query->row();
		
		$v_data['event_location'] = $this->event_location.$event_row->event_partners_image_name;
		
		$this->session->unset_userdata('event_error_message');
		
		//upload image if it has been selected
		$response = $this->event_model->upload_event_partner_image($this->event_path, $event_row->event_partners_image_name);
		if($response)
		{
			$v_data['event_location'] = $this->event_location.$this->session->userdata('event_partner_file_name');
		}
		
		//case of upload error
		else
		{
			$event_error = $this->session->userdata('event_error_message');
			$this->session->set_userdata('error_message', $event_error);
			$this->session->unset_userdata('event_error_message');
		}
		
		$event_error = $this->session->userdata('event_error_message');
		
		$this->form_validation->set_rules('event_partners_name', 'Name', 'required|xss_clean');
		$this->form_validation->set_rules('event_partners_location', 'Website URL', 'trim|xss_clean');
		$this->form_validation->set_rules('event_partners_type', 'Type', 'trim|xss_clean');
		$this->form_validation->set_rules('event_partners_description', 'Description', 'xss_clean');

		if ($this->form_validation->run())
		{	
			if(empty($event_error))
			{
				$event = $this->session->userdata('event_partner_file_name');
				
				if(($event == FALSE) || empty($event))
				{
					$event = $event_row->event_partners_image_name;
				}
				$data2 = array(
					'event_partners_name'=>$this->input->post("event_partners_name"),
					'event_partners_link'=>$this->input->post("event_partners_location"),
					'event_partners_type'=>$this->input->post("event_partners_type"),
					'event_partners_description'=>$this->input->post("event_partners_description"),
					'event_partners_image_name'=>$event,
					'event_id'=>$event_id,
				);
				
				$table = "event_partners";
				$this->db->where($where);
				if($this->db->update($table, $data2))
				{
					$this->session->unset_userdata('event_partner_file_name');
					$this->session->unset_userdata('event_partner_thumb_name');
					$this->session->unset_userdata('event_error_message');
					$this->session->set_userdata('success_message', 'Event partner has been edited');
					
					redirect('admin/events/edit_event_partners/'.$event_id.'/'.$event_partners_id.'/'.$page);
				}
				
				else
				{
					$this->session->set_userdata('error_message', 'Event partner could not be edited');
				}
			}
		}
		
		$v_data['page'] = $page;
		$v_data['event_id'] = $event_id;
		$v_data['event_location_parnters'] = $this->event_location;
		$v_data['event_partners'] = $events_query;
		
		$data['content'] = $this->load->view("event/edit_event_partner", $v_data, TRUE);
		$data['title'] = 'Edit Event';
		
		$this->load->view('templates/general_page', $data);
	}
}
?>