<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/admin/controllers/admin.php";

class Resource extends admin {
	var $resource_path;
	var $resource_location;
	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('users_model');
		$this->load->model('resources_model');
		$this->load->model('file_model');
		$this->load->library('image_lib');
		
		//path to image directory
		$this->resource_path = realpath(APPPATH . '../assets/resources');
		$this->resource_location = base_url().'assets/resources/';
	}
    
	/*
	*
	*	Default action is to show all the registered resource
	*
	*/
	public function index() 
	{
		$where = 'resource_id > 0';
		$table = 'resources';
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'resource-files';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 5;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$v_data['title'] = 'All Resources';
        $v_data['resource_location']=$this->resource_location = base_url().'assets/resources/';
		$v_data["links"] = $this->pagination->create_links();
		$query = $this->resources_model->get_all_resources($table, $where, $config["per_page"]);
		if ($query->num_rows() > 0)
		{
			$v_data['query'] = $query;
			$data['content'] = $this->load->view('resources/all_resources', $v_data, true);
		}
		else {
			$data['content'] = '<div class="container py-4">
										<div class="card bg-light">
											<div class="p-5 mb-4 bg-light rounded-3">
												<div class="container-fluid py-5">
													<h1 class="display-5 font-weight-bold">No Resources to display</h1>
													<hr>
													<p class="lead">Click the add resource button to add a new resource</p>
													<a class="btn btn-primary btn-lg" href="'.site_url().'resources/add-resource">Add Resource</a>
												</div>
											</div>
										</div>
									</div>';
			$data['title'] = $v_data['title'] = 'All Resources';
		}
		$this->load->view('templates/general_page', $data);
	}
	
	function add_resource()
	{
		$v_data['resource_location'] = 'https://www.orimi.com/pdf-test.pdf';
		$this->session->unset_userdata('resource_error_message');
		//upload image if it has been selected
		$response = $this->resources_model->upload_resource_document($this->resource_path);
		if($response)
		{
			$v_data['resource_location'] = $this->resource_location.$this->session->userdata('resource_file_name');
		}
		//case of upload error
		else
		{
			$v_data['resource_error'] = $this->session->userdata('resource_error_message');
		}
		$resource_error = $this->session->userdata('resource_error_message');
		$this->form_validation->set_rules('resource_title', 'Title', 'trim|xss_clean');
		if ($this->form_validation->run())
		{	
			if(empty($resource_error))
			{
				$data2 = array(
					'resource_title'=>$this->input->post("resource_title"),
					'resource_name'=>$this->session->userdata('resource_file_name'),
					'resource_status'=>1
				);		
				$table = "resources";
				$this->db->insert($table, $data2);
				$this->session->unset_userdata('resource_file_name');
				$this->session->unset_userdata('resource_error_message');
				$this->session->set_userdata('success_message', 'The resource was added successfully');
				redirect('resource-files');
			}
		}
		$table = "resources";
		$where = "resource_id != 0";
		$this->db->where($where);
		$v_data['resources'] = $this->db->get($table);
		$resources = $this->session->userdata('resource_file_name');
		if(!empty($resources))
		{
			$v_data['resource_location'] = $this->resource_location.$this->session->userdata('resource_file_name');
		}
		$v_data['error'] = $resource_error;
		$data['title'] = $v_data['title'] = 'Add New Resource';
		$data['content'] = $this->load->view("resources/add_resource", $v_data, TRUE);
		$this->load->view('templates/general_page', $data);
	}
	
	// Edit Resource
	function edit_resource($resource_id)
	{
		// echo "Here it is";
		$table = "resources";
		$where = "resource_id = ".$resource_id;
		$this->db->where($where);
		$resources_query = $this->db->get($table);
		$resource_row = $resources_query->row();
		$v_data['resource_row'] = $resource_row;
		$v_data['resource_location'] = $this->resource_location.$resource_row->resource_name;
		$this->session->unset_userdata('resource_error_message');
		//upload image if it has been selected
		$response = $this->resources_model->upload_resource_document($this->resource_path, $edit = $resource_row->resource_name);
		if($response)
		{
			$v_data['resource_location'] = $this->resource_location.$this->session->userdata('resource_file_name');
		}
		//case of upload error
		else
		{
			$v_data['resource_error'] = $this->session->userdata('resource_error_message');
		}
		$resource_error = $this->session->userdata('resource_error_message');
		$this->form_validation->set_rules('check', 'check', 'trim|xss_clean');
		$this->form_validation->set_rules('resource_name', 'Title', 'trim|xss_clean');
		$this->form_validation->set_rules('resource_title', 'Title', 'trim|xss_clean');
		if ($this->form_validation->run())
		{	
			if(empty($resource_error))
			{
				$resource = $this->session->userdata('resource_file_name');
				if($resource == FALSE)
				{
					$resource = $resource_row->resource_name;
				}
				$data = array(
					'resource_title'=>$this->input->post("resource_title"),
					'resource_name'=>$resource				
				);		
				$table = "resources";
				$this->db->where('resource_id', $resource_id);
				$this->db->update($table, $data);
				$this->session->unset_userdata('resource_file_name');
				$this->session->unset_userdata('resource_thumb_name');
				$this->session->unset_userdata('resource_error_message');
				$this->session->set_userdata('success_message', 'The resource has been edited');
				redirect('resource-files/');
			}
		}
		$resource = $this->session->userdata('resource_file_name');
		if(!empty($resource))
		{
			$v_data['resource_location'] = $this->resource_location.$this->session->userdata('resource_file_name');
		}
		$v_data['error'] = $resource_error;
		$data['title'] = $v_data['title'] = 'Edit Resource';
		$data['content'] = $this->load->view("resources/edit_resource", $v_data, TRUE);
		$this->load->view('templates/general_page', $data);
	}
    
	/*
	*
	*	Delete an existing resource
	*	@param int $resource_id
	*
	*/
	function delete_resource($resource_id)
	{
		//get resource data
		$table = "resources";
		$where = "resource_id = ".$resource_id;
		$this->db->where($where);
		$slides_query = $this->db->get($table);
		$slide_row = $slides_query->row();
		$resource_path = $this->resource_path;		
		$image_name = $slide_row->resource_name;
		//delete any other uploaded image
		$this->file_model->delete_file($resource_path."\\".$image_name, $this->resource_path);
		//delete any other uploaded thumbnail
		$this->file_model->delete_file($resource_path."\\thumbnail_".$image_name, $this->resource_path);
		if($this->resources_model->delete_resource($resource_id))
		{
			$this->session->set_userdata('success_message', 'The resource has been deleted');
		}
		else
		{
			$this->session->set_userdata('error_message', 'The resource could not be deleted');
		}
		redirect('resource-files/');
	}
    
	/*
	*
	*	Activate an existing resource
	*	@param int $resource_id
	*
	*/
	public function activate_resource($resource_id)
	{
		if($this->resources_model->activate_resource($resource_id))
		{
			$this->session->set_userdata('success_message', 'The resource has been activated');
		}
		else
		{
			$this->session->set_userdata('error_message', 'The resource could not be activated');
		}
		redirect('resource-files/');
	}
    
	/*
	*
	*	Deactivate an existing resource
	*	@param int $resource_id
	*
	*/
	public function deactivate_resource($resource_id)
	{
		if($this->resources_model->deactivate_resource($resource_id))
		{
			$this->session->set_userdata('success_message', 'The resource has been deactivated');
		}
		else
		{
			$this->session->set_userdata('error_message', 'The resource could not be deactivated');
		}
		redirect('resource-files/');
	}
}
?>