<?php

class Branches_model extends CI_Model 
{	
	/*
	*	Retrieve all branches
	*
	*/
	public function all_branches()
	{
		$this->db->order_by('branch_name');
		$this->db->where('branch_status = 1');
		$query = $this->db->get('branch');
		
		return $query;
	}
	/*
	*	Retrieve latest branch
	*
	*/
	public function latest_branch()
	{
		$this->db->limit(1);
		$this->db->order_by('created', 'DESC');
		$query = $this->db->get('branch');
		
		return $query;
	}
	/*
	*	Retrieve all parent branches
	*
	*/
	public function all_parent_branches()
	{
		$this->db->where('branch_status = 1 AND branch_parent = 0');
		$this->db->order_by('branch_name', 'ASC');
		$query = $this->db->get('branch');
		
		return $query;
	}
	/*
	*	Retrieve all children branches
	*
	*/
	public function all_child_branches()
	{
		$this->db->where('branch_status = 1 AND branch_parent > 0');
		$this->db->order_by('branch_name', 'ASC');
		$query = $this->db->get('branch');
		
		return $query;
	}
	
	/*
	*	Retrieve all branches
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_branches($table, $where, $per_page, $page, $order = 'branch_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	/*
	*	Add a new branch
	*	@param string $image_name
	*
	*/
	public function add_branch($image_name, $thumb_name)
	{
		$data = array(
				'branch_code'		=> $this->input->post('branch_code'),
				'branch_name'		=> $this->input->post('branch_name'),
				'branch_parent'		=> $this->input->post('branch_parent'),
				'branch_status'		=> $this->input->post('branch_status'),
				'branch_phone'		=> $this->input->post('branch_phone'),
				'branch_email'		=> $this->input->post('branch_email'),
				'branch_working_weekday'=> $this->input->post('branch_working_weekday'),
				'branch_working_weekend'=> $this->input->post('branch_working_weekend'),
				'branch_address'	=> $this->input->post('branch_address'),
				'branch_city'		=> $this->input->post('branch_city'),
				'branch_post_code'	=> $this->input->post('branch_post_code'),
				'branch_location'	=> $this->input->post('branch_location'),
				'branch_building'	=> $this->input->post('branch_building'),
				'branch_floor'		=> $this->input->post('branch_floor'),
				'created'			=> date('Y-m-d H:i:s'),
				'created_by'		=> $this->session->userdata('personnel_id'),
				'modified_by'		=> $this->session->userdata('personnel_id'),
				'branch_image_name'	=>$image_name,
				'branch_thumb_name'	=>$thumb_name,
				'location_link'	=>$this->input->post('location_link')
			);
		if($this->db->insert('branch', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Update an existing branch
	*	@param string $image_name
	*	@param int $branch_id
	*
	*/
	public function update_branch($image_name, $thumb_name, $branch_id)
	{
		$data = array(
				'branch_code'		=> $this->input->post('branch_code'),
				'branch_name'		=> $this->input->post('branch_name'),
				'branch_parent'		=> $this->input->post('branch_parent'),
				'branch_status'		=> $this->input->post('branch_status'),
				'branch_phone'		=> $this->input->post('branch_phone'),
				'branch_email'		=> $this->input->post('branch_email'),
				'branch_working_weekday'=> $this->input->post('branch_working_weekday'),
				'branch_working_weekend'=> $this->input->post('branch_working_weekend'),
				'branch_address'	=> $this->input->post('branch_address'),
				'branch_city'		=> $this->input->post('branch_city'),
				'branch_post_code'	=> $this->input->post('branch_post_code'),
				'branch_location'	=> $this->input->post('branch_location'),
				'branch_building'	=> $this->input->post('branch_building'),
				'branch_floor'		=> $this->input->post('branch_floor'),
				'modified_by'		=> $this->session->userdata('personnel_id'),
				'branch_image_name'	=>$image_name,
				'branch_thumb_name'	=>$thumb_name,
				'location_link'	=>$this->input->post('location_link')
			);
			
		$this->db->where('branch_id', $branch_id);
		if($this->db->update('branch', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single branch's children
	*	@param int $branch_id
	*
	*/
	public function get_sub_branches($branch_id)
	{
		//retrieve all users
		$this->db->from('branch');
		$this->db->select('*');
		$this->db->where('branch_parent = '.$branch_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	get a single branch's details
	*	@param int $branch_id
	*
	*/
	public function get_branch($branch_id)
	{
		//retrieve all users
		$this->db->from('branch');
		$this->db->select('*');
		$this->db->where('branch_id = '.$branch_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	Delete an existing branch
	*	@param int $branch_id
	*
	*/
	public function delete_branch($branch_id)
	{
		if($this->db->delete('branch', array('branch_id' => $branch_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Activate a deactivated branch
	*	@param int $branch_id
	*
	*/
	public function activate_branch($branch_id)
	{
		$data = array(
				'branch_status' => 1
			);
		$this->db->where('branch_id', $branch_id);
		

		if($this->db->update('branch', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated branch
	*	@param int $branch_id
	*
	*/
	public function deactivate_branch($branch_id)
	{
		$data = array(
				'branch_status' => 0
			);
		$this->db->where('branch_id', $branch_id);
		
		if($this->db->update('branch', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function upload_branch_logo($branch_path, $branch_location, $edit = NULL)
	{
		$resize['width'] = 200;
		$resize['height'] = 200;
		
		
		if(!empty($_FILES['branch_image']['tmp_name']))
		{

			$image = $this->session->userdata('branch_file_name');
			
			if((!empty($image)) || ($edit != NULL))
			{
				if($edit != NULL)
				{
					$image = $edit;
				}
				
				//delete any other uploaded image
				if($this->file_model->delete_file($branch_path."\\".$image, $branch_location))
				{
					//delete any other uploaded thumbnail
					$this->file_model->delete_file($branch_path."\\thumbnail_".$image, $branch_location);
				}
				
				else
				{
					$this->file_model->delete_file($branch_path."/".$image, $branch_location);
					$this->file_model->delete_file($branch_path."/thumbnail_".$image, $branch_location);
				}
			}
			//Upload image


			$response = $this->file_model->upload_file($branch_path, 'branch_image', $resize);
			if($response['check'])
			{
				$file_name = $response['file_name'];
				$thumb_name = $response['thumb_name'];
				
				//Set sessions for the image details
				$this->session->set_userdata('branch_file_name', $file_name);
				$this->session->set_userdata('branch_thumb_name', $thumb_name);
			
				return TRUE;
			}
		
			else
			{
				$this->session->set_userdata('branch_error_message', $response['error']);
				
				return FALSE;
			}
		}
		
		else
		{
			$this->session->set_userdata('branch_error_message', '');
			return FALSE;
		}
	}
	
	//branch import temolate
	//import template
	function import_branches_template()
	{
		$this->load->library('Excel');
		
		$title = 'Branches Import Template';
		$count=1;
		$row_count=0;
		
		$branch[$row_count][0] = 'Branch Name';
		$branch[$row_count][1] = 'Branch Email';
		$branch[$row_count][2] = 'Branch Phone';
		$branch[$row_count][3] = 'Branch Address';
		$branch[$row_count][4] = 'Branch Post Code';
		$branch[$row_count][5] = 'Branch Building';
		$branch[$row_count][6] = 'Branch Floor';
		$branch[$row_count][7] = 'Branch Location';
		$branch[$row_count][8] = 'Branch City';
		$branch[$row_count][9] = 'Branch KRA PIN';
		
		$row_count++;
		
		//create the excel document
		$this->excel->addArray ( $branch );
		$this->excel->generateXML ($title);
	}
	
	//import branch
	public function import_csv_branch($upload_path)
	{
		//load the file model
		$this->load->model('admin/file_model');
		/*
			-----------------------------------------------------------------------------------------
			Upload csv
			-----------------------------------------------------------------------------------------
		*/
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');
		
		if($response['check'])
		{
			$file_name = $response['file_name'];
			
			$array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			//var_dump($array); die();
			$response2 = $this->sort_branch_data($array);
		
			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}
			
			return $response2;
		}
		
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	//sorting the imported branch data
	public function sort_branch_data($array)
	{
		//count total rows
		$total_rows = count($array);
		$total_columns = count($array[0]);//var_dump($array);die();
		
		//if branch exist in array
		if(($total_rows > 0) && ($total_columns == 10))
		{
			$items['modified_by'] = $this->session->userdata('personnel_id');
			$items['created_by'] = $this->session->userdata('personnel_id');
			$items['created'] = date('Y-m-d H:i:s');
			$response = '
				<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Branch Name</th>
						  <th>Branch Email</th>
						  <th>Branch Address</th>
						  <th>Location</th>
						  <th>Comment</th>
						</tr>
					  </thead>
					  <tbody>
			';
			
			//retrieve the data from array
			for($r = 1; $r < $total_rows; $r++)
			{
				$items['branch_name']=$array[$r][0];
				$items['branch_email']=$array[$r][1];
				$items['branch_phone']=$array[$r][2];
				$items['branch_address']=$array[$r][3];
				$items['branch_post_code']=$array[$r][4];
				$items['branch_building']=$array[$r][5];
				$items['branch_floor']=$array[$r][6];
				$items['branch_location']=$array[$r][7];
				$items['branch_city']=$array[$r][8];
				$items['branch_kra_pin']=$array[$r][9];
				$comment = '';
				if(!empty($items['branch_name']))
				{
						if($this->db->insert('branch', $items))
						{
							$comment .= '<br/>Branch successfully added to the database';
							$class = 'success';
						}
						
						else
						{
							$comment .= '<br/>Internal error. Could not add branch to the database. Please contact the site administrator';
							$class = 'warning';
						}
				}
				
				else
				{
					$comment .= '<br/>Not saved ensure you have a branch name entered';
					$class = 'danger';
				}
				
				
				$response .= '
					
						<tr class="'.$class.'">
							<td>'.$r.'</td>
							<td>'.$items['branch_name'].'</td>
							<td>'.$items['branch_email'].'</td>
							<td>'.$items['branch_address'].' - '.$items['branch_post_code'].'</td>
							<td>'.$items['branch_location'].'</td>
							<td>'.$comment.'</td>
						</tr> 
				';
			}
			
			$response .= '</table>';
			
			$return['response'] = $response;
			$return['check'] = TRUE;
		}
		
		//if no branch exist
		else
		{
			$return['response'] = 'Branch data not found ';
			$return['check'] = FALSE;
		}
		
		return $return;
	}
	
}
?>