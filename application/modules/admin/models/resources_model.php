<?php

class Resources_model extends CI_Model 
{	
	public function upload_resource_document($resource_path, $edit = NULL)
	{
		//upload product's gallery images
		// $resize['width'] = 1920;
		// $resize['height'] = 1200;
		
		if(!empty($_FILES['resource_file']['tmp_name']))
		{
			$file = $this->session->userdata('resource_file_name');
			
			if((!empty($image)) || ($edit != NULL))
			{
				if($edit != NULL)
				{
					$image = $edit;
				}
				//delete any other uploaded image
				$this->file_model->delete_file($resource_path."\\".$file, $resource_path);
				
				//delete any other uploaded thumbnail
				$this->file_model->delete_file($resource_path."\\resource_".$file, $resource_path);
			}
			//Upload image
			$response = $this->file_model->upload_file($resource_path, 'resource_file', $resize);//var_dump($response);die();
			if($response['check'])
			{
				$file_name = $response['file_name'];
				$thumb_name = $response['thumb_name'];
				//crop file to 1920 by 1010
				$this->session->set_userdata('resource_file_name', $file_name);
				$this->session->set_userdata('resource_thumb_name', $thumb_name);
				return TRUE;
			}
		
			else
			{
				$this->session->set_userdata('error_message', 'Cannot upload image. '.$response['error']);
				return FALSE;
			}
		}
		else
		{
			$this->session->set_userdata('error_message', '');
			return FALSE;
		}
	}
	
	// public function get_all_resources($table, $where, $per_page, $page)
	// {
	// 	$this->db->from($table);
	// 	$this->db->select('*');
	// 	$this->db->where($where);
	// 	$this->db->order_by('resource_id');
	// 	$query = $this->db->get('', $per_page, $page);
	// 	return $query;
	// }
	public function get_all_resources($table, $where, $per_page)
	{
		//retrieve all gallerys
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		// $this->db->join('department','department.department_id = gallery.department_id','left');
		// $this->db->order_by('department.department_name');
		$this->db->order_by('resource_id');
		$query = $this->db->get('', $per_page);
		
		return $query;
	}
	
	
	/*
	*	Delete an existing slideshow
	*	@param int $slideshow_id
	*
	*/
	public function delete_resource($resource_id)
	{
		if($this->db->delete('resources', array('resource_id' => $resource_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Activate a deactivated slideshow
	*	@param int $slideshow_id
	*
	*/
	public function activate_resource($resource_id)
	{
		$data = array(
				'resource_status' => 1
			);
		$this->db->where('resource_id', $resource_id);
		
		if($this->db->update('resources', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activate resource
	*	@param int $resource_id
	*
	*/
	public function deactivate_resource($resource_id)
	{
		$data = array(
				'resource_status' => 0
			);
		$this->db->where('resource_id', $resource_id);
		
		if($this->db->update('resources', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
}
