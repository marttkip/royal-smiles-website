
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Search <?php echo $title;?></h2>
    </header>
    <div class="panel-body">
    	<?php
        echo form_open('admin/blog/search_group_post');
		?>
        <div class="row">
        	<div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-4">Category</label>
                    <div class="col-md-8">
                        <?php
                        if($categories_query->num_rows > 0)
						{
							$categories = '<select class="form-control" name="post_category">';
							$categories .= '<option value="">--Search Category--</option>';
							
							foreach($categories_query->result() as $res)
							{
								$categories .= '<option value="'.$res->blog_category_id.'">'.$res->blog_category_name.'</option>';
							}
							$categories .= '</select>';
							
							echo $categories;
						}
						?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-4">Post Title</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="post_title" placeholder="Post Title"/>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-4 col-md-offset-5">
            	<button type="submit" class="btn btn-primary">Search</button>
            </div>
            <?php
				$search = $this->session->userdata('group_post_search');
				
				if(!empty($search))
				{
				   ?>
					<div class="col-md-4">
                    	<a href="<?php echo site_url().'admin/blog/close_group_post_search';?>" class="btn btn-warning">Close Search</a>
					</div>
                    <?php
				}
            ?>
        </div>
        <?php
		echo form_close();
		?>
    </div>
</section>

  <section class="panel">
        <header class="panel-heading">
            <div class="pull-right">
                <a href="<?php echo site_url();?>add-post/<?php echo $title_url;?>" class="btn btn-sm btn-success pull-right"><i class="fa fa-plus"></i> Add Post</a>
            </div>
    
            <h2 class="panel-title"><?php echo $title;?></h2>
        </header>
        <div class="panel-body">

		<?php
				
				$result = '';
		            
				$success = $this->session->userdata('success_message');
				
				if(!empty($success))
				{
					echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
					$this->session->unset_userdata('success_message');
				}
				
				$error = $this->session->userdata('error_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
					$this->session->unset_userdata('error_message');
				}
				
				//if users exist display them
				if ($query->num_rows() > 0)
				{
					$count = $page;
					
					$result .= 
					'
						<table class="table table-hover table-bordered table-responsive">
						  <thead>
							<tr>
							  <th>#</th>
							  <th>Image</th>
							  <th>Category</th>
							  <th>Post Title</th>
							  <th>Date Created</th>
							  <th>Views</th>
							  <th>Comments</th>
							  <th>Status</th>
							  <th colspan="5">Actions</th>
							</tr>
						  </thead>
						  <tbody>
					';
					
					//get all administrators
					$administrators = $this->users_model->get_all_administrators();
					if ($administrators->num_rows() > 0)
					{
						$admins = $administrators->result();
					}
					
					else
					{
						$admins = NULL;
					}
					
					foreach ($query->result() as $row)
					{
						$post_id = $row->post_id;
						$blog_category_name = $row->blog_category_name;
						$post_title = $row->post_title;
						$post_status = $row->post_status;
						$post_views = $row->post_views;
						$image = $row->post_image;
						$created_by = $row->created_by;
						$modified_by = $row->modified_by;
						$comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
					
						//$web_name = $this->site_model->create_web_name($post_title);
						//status
						if($post_status == 1)
						{
							$status = 'Active';
						}
						else
						{
							$status = 'Disabled';
						}
						
						//create deactivated status display
						if($post_status == 0)
						{
							$status = '<span class="label label-danger">Deactivated</span>';
							$button = '<a class="btn btn-info" href="'.site_url().'activate-post/'.$post_id.'/'.$title_url.'" onclick="return confirm(\'Do you want to activate '.$post_title.'?\');">Activate</a>';
						}
						//create activated status display
						else if($post_status == 1)
						{
							$status = '<span class="label label-success">Active</span>';
							$button = '<a class="btn btn-info" href="'.site_url().'deactivate-post/'.$post_id.'/'.$title_url.'" onclick="return confirm(\'Do you want to deactivate '.$post_title.'?\');">Deactivate</a>';
						}
						
						
						$count++;
						$result .= 
						'
							<tr>
								<td>'.$count.'</td>
								<td><img src="'.base_url()."assets/images/posts/".$image.'" style=" width: 50px !important;"></td>
								<td>'.$blog_category_name.'</td>
								<td>'.$post_title.'</td>
								<td>'.date('jS M Y',strtotime($row->created)).'</td>
								<td>'.$post_views.'</td>
								<td>'.$comments.'</td>
								<td>'.$status.'</td>
								<td><a href="'.site_url().'edit-post/'.$post_id.'/'.$title_url.'" class="btn btn-sm btn-success">Edit</a></td>
								<td><a href="'.site_url().'comments/'.$post_id.'/'.$title_url.'" class="btn btn-sm btn-warning">Comments</a></td>
								<td>'.$button.'</td>
								<td><a href="'.site_url().'delete-post/'.$post_id.'/'.$title_url.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$post_title.'? This will also delete all comments associated with this post\');">Delete</a></td>
							</tr> 
						';
					}
					
					$result .= 
					'
								  </tbody>
								</table>
					';
				}
				
				else
				{
					$result .= "There are no posts";
				}
				
				echo $result;
		?>
		<?php if(isset($links)){echo $links;}?>
	</div>
</section>