  <section class="panel">
        <header class="panel-heading">
            <div class="pull-right">
                <a href="<?php echo site_url();?>web-content/<?php echo $title_url;?>" class="btn btn-sm btn-default pull-right"><i class="fa fa-arrow-left"></i> Back to posts</a>
            </div>
    
            <h2 class="panel-title"><?php echo $title;?></h2>
        </header>
        <div class="panel-body">
          <style type="text/css">
		  	.add-on{cursor:pointer;}
		  </style>
          <link href="<?php echo base_url()."assets/themes/jasny/css/jasny-bootstrap.css"?>" rel="stylesheet"/>
          <div class="padd">
            <!-- Adding Errors -->
            <?php
            if(isset($error)){
                echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
            }
			
			//the post details
			$post_id = $post[0]->post_id;
			$post_title = $post[0]->post_title;
			$post_status = $post[0]->post_status;
			$post_content = $post[0]->post_content;
            $post_video = $post[0]->post_video;
            $post_target = $post[0]->post_target;
			$image = $post[0]->post_image;
			$created = $post[0]->post_date;
			$header_image = $post[0]->post_header;
            $featured_status = $post[0]->featured_status;
            $post_meta = $post[0]->post_meta;

			$attachment_name = $post[0]->attachment_name;
            
            $validation_errors = validation_errors();
            
            if(!empty($validation_errors))
            {
				$post_title = set_value('post_title');
				$post_status = set_value('post_status');
				$post_content = set_value('post_content');
                $post_video = set_value('post_video');
                $post_target = set_value('post_target');
                $featured_status = set_value('featured_status');
				$created = set_value('created');
				
                echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
            }
			
            ?>
            
            <?php echo form_open_multipart($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                           <!-- post category -->
                        <div class="form-group">
                            <label class="col-md-4 control-label">Post Category</label>
                            <div class="col-md-8">
                                <?php echo $categories;?>
                            </div>
                        </div>
                        <!-- post Name -->
                        <div class="form-group">
                            <label class="col-md-4 control-label">Post Title</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="post_title" placeholder="Post Title" value="<?php echo $post_title;?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Post Video Link</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="post_video" placeholder="Post video" value="<?php echo $post_video;?>" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Post Target (KSH)</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="post_target" placeholder="Target" value="<?php echo $post_target;?>" >
                            </div>
                        </div>
                        
                         <!-- Activate checkbox -->
                        <div class="form-group">
                            <label class="col-md-4 control-label">Activate Post?</label>
                            <div class="col-md-4">
                                <div class="radio">
                                    <label>
                                        <?php
                                        if($post_status == 1){echo '<input id="optionsRadios1" type="radio" checked value="1" name="post_status">';}
                                        else{echo '<input id="optionsRadios1" type="radio" value="1" name="post_status">';}
                                        ?>
                                        Yes
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <?php
                                        if($post_status == 0){echo '<input id="optionsRadios1" type="radio" checked value="0" name="post_status">';}
                                        else{echo '<input id="optionsRadios1" type="radio" value="0" name="post_status">';}
                                        ?>
                                        No
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Featured ?</label>
                            <div class="col-md-4">
                                <div class="radio">
                                    <label>
                                        <?php
                                        if($featured_status == 1){echo '<input id="optionsRadios1" type="radio" checked value="1" name="featured_status">';}
                                        else{echo '<input id="optionsRadios1" type="radio" value="1" name="featured_status">';}
                                        ?>
                                        Yes
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <?php
                                        if($featured_status == 0){echo '<input id="optionsRadios1" type="radio" checked value="0" name="featured_status">';}
                                        else{echo '<input id="optionsRadios1" type="radio" value="0" name="featured_status">';}
                                        ?>
                                        No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                    	<div class="form-group">
                            <label class="col-md-4 control-label">Post Date: </label>
                            
                            <div class="col-md-8">
                            	<div class="input-group date">
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                    <input id="datapicker2"  name="created" placeholder="Post Date" value="<?php echo $created;?>" class="form-control" type="text">
                                </div>
                            </div>
                        </div>
                        <!-- Image -->
                        <div class="form-group">
                            <label class="col-md-4 control-label">Post Image</label>
                            <input type="hidden" value="<?php echo $image;?>" name="current_image"/>
                            <div class="col-md-4">
                                
                                <div class="row">
                                
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width:200px; height:200px;">
                                                <img src="<?php echo base_url()."assets/images/posts/".$image;?>">
                                            </div>
                                            <div>
                                                <span class="btn btn-file btn-info"><span class="fileinput-new">Select Image</span><span class="fileinput-exists">Change</span><input type="file" name="post_image"></span>
                                                <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                         <label class="col-md-4 control-label" for="image">Gallery Image</label>
                        <div class="col-md-8">
                        <?php echo form_upload(array( 'name'=>'gallery[]', 'multiple'=>true, 'class'=>'btn btn-danger'));?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                         <label class="col-md-4 control-label" for="image">Header Image</label>
                         <input type="hidden" value="<?php echo $header_image;?>" name="current_header_image"/>
                        <div class="col-md-8">
                        	<div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width:200px; height:200px;">
                                    <img src="<?php echo base_url()."assets/images/posts/".$header_image;?>">
                                </div>
                                <div>
                                    <span class="btn btn-file btn-info"><span class="fileinput-new">Select Image</span><span class="fileinput-exists">Change</span><input type="file" name="header_image"></span>
                                    <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                         <label class="col-md-4 control-label" for="image">Attachment</label>
                        <div class="col-md-8">
                        <?php echo form_upload(array( 'name'=>'attachment', 'class'=>'btn btn-warning'));?>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="hpanel">
                        <div class="panel-heading">
                           Gallery Images
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <?php
								$gallery_images = '';
								
								if($post_gallery->num_rows() > 0)
								{
									foreach($post_gallery->result() as $res)
									{
										$post_gallery_id = $res->post_gallery_id;
										$post_gallery_image_name = $res->post_gallery_image_name;
										$gallery_images .= '
											<div class="col-md-4">
												<img src="'.base_url().'assets/images/posts/'.$post_gallery_image_name.'" class="img-responsive"/><a class="btn btn-danger" href="'.site_url().'admin/blog/delete_post_gallery/'.$post_gallery_id.'/'.$post_id.'" onclick="return confirm(\'Are you sure you want to delete this image\')"><i class="fa fa-trash"></i> Delete</a>
											</div>
										';
									}
								}
								
								echo $gallery_images;
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

            <div class="row">
                <div class="col-md-12">
                    <div class="hpanel">
                        <div class="panel-heading">
                           Post Content
                        </div>
                      
                        <textarea  id="post_content" name="post_content" class="cleditor">  <?php echo $post_content;?></textarea>


                        <div class="row" style="margin-top: 20px;margin-bottom: 20px;">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-1 control-label">Post Meta</label>
                                    <div class="col-md-11">
                                        <input type="text" class="form-control" name="post_meta" placeholder="Smart smile, Dental Clinic" value="<?php echo $post_meta;?>" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            <div class="row">
                <div class="col-md-12">
                      <div class="form-actions center-align">
                        <button class="submit btn btn-primary" onclick="save_items_content()" id="save-button" type="submit">
                            Edit post
                        </button>
                    </div>
                </div>
            </div>
            <br />
            <?php echo form_close();?>
		</div>
    </div>
</section>
<script >
      function save_items_content()
      {
        var mysave = $('div.note-editable').html();
        // $('textarea.note-editable').val(mysave);
        $('textarea#post_content').val(mysave);
      }
	  $(document).ready(function(){
		  //$('#datapicker2').datepicker({format: 'yyyy-mm-dd'});
        $(function(){
            $('#save-button').click(function () {

                var mysave = $('div.note-editable').html();
                // alert(mysave);
                // $('textarea.note-editable').val(mysave);
                $('textarea#post_content').val(mysave);
              });
            });
     });
</script>