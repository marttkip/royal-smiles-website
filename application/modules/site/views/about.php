<!--Page header & Title-->
<section id="page_header">
<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">About Us</h2>
         <div class="page_link"><a href="index.html">Home</a><i class="fa fa-long-arrow-right"></i><span>About Us</span></div>
      </div>
    </div>
  </div>
</div>  
</section>



<!--Welcome-->
<section id="welcome" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="heading">Welcome to Glogoh Hospital</h2>
         <hr class="heading_space">
      </div>
      <div class="col-md-7 col-sm-6">
Glogoh Hospital hopes to be a center of excellence in provision of evidence based quality healthcare services including care, training and research. It seeks to establish a Global Group of Hospitals (GLOGOH) that will be available in every country in the world. It will be privately owned but working with individual citizens, communities, nations, regions and the world to provide international standard of care in all countries regardless of the economic status of the country the Glogoh hospitals will be based. Its services shall be based on the fact that “Health is a state of complete physical, mental and social well-being and not merely the absence of disease or infirmity”. It is, therefore, the stand of Glogoh hospital that health is the penultimate product of all human and non-human activities. Glogoh shall practice some of its values as follows:
  - Use nothing but the GOLDEN RULE and the TRUTH in every service we offer unless it is not in the interest of our customers. Examples: brucellosis, typhoid fever & albinism, sickle cell disease, Down’s syndrome & genetic disorders
 - Allow our customers to be at LIBERTY to make informed choices unless their choices contravene rules, regulations and/or laws governing healthcare.
 - Promote and sustain hard and smart WORKING by all involved in our service delivery especially our personnel and customers. E.g healthy diets and weights
 - Advocate for promotion of EQUITY for sustainable social development.

HISTORY:
1.  2006: group of doctors met and thought there was need to have private hospital owned and managed by doctors . Consequently Dr. Lelei, myself and others went round Eldoret looking for potential site for said hospital
2.  2009 -2010 purchase current Glogoh hospital plot for hospital construction.
3.  2010-2012 form a group and purchase land next to St Lukes establishing hospital.  However the group does not raise enough funds for construction and, in 2016, two members of the group pull out to establish their own hospitals.
4.  November 2017 Glogoh Hospital buildings are ready for use but the we are not sure of services to offer; 
5.  2018: wanted to sell property but our 3 sons advise us not to dispose it.
6.  Today we are dedicating Glogoh Hospital to our 3 sons, Steve, Sammy and Shem, for them to serve themselves, their future families and their fellow humans. 
SOME FACTS:
-  1948 WHO definition of health: PHYSICAL, MENTAL & SOCIAL WELLBEING.
-  Global health =holistic health for individual, family, society, nations & the world.
-  Not all human beings (men) are born equal hence need for the Golden Rule,etc
-  Health =penultimate product of all activities; human and natural.
-  Today’s individual is globally connected socially, physically, economically, etc.
-  Social wellbeing or lack of is a major contributor of the state of healthcare everywhere at all levels from individual to global community yet it has been inadequately provided. Glogoh hopes to focus on this aspect of healthcare at the hospital level and beyond.
GLOGOH HOSPITAL prefers:
 I.  The augmentable National Health Insurance Model: single-payer national health insurance to:
II. The Beveridge Model: single-payer national health service
III.  The Bismarck Model: social health insurance model
IV. The Out-of-Pocket Model: market-driven health care
V.  Mixture of I to IV 
Reasons:
 Government provides policy, standards and regulations
 Providers such as Glogoh Hospital compete for customers and, therefore, funding leading to continuous quality improvement of wellbeing of both the providers and recipients of healthcare.
Hence the need for network of hospitals to standardize practice globally, increase customer numbers and improve on universal health coverage
This calls for a universally acceptable global governance of social services such as healthcare and education. Glogoh Hospital hopes to be an active participant of the improvement of existing global governance of and for social welfare by using current and developing new evidence to inform global policy and practice for the common good of mankind. 
Of course this sounds like utopia. But many dreams have been regarded as utopia such as a man landing in the moon or black Americans being allowed to vote in 1965, 189 years after the first USA President G. Washington, declared in 1776 that “all men are born equal with certain inalienable rights”. On behalf of Glogoh Hospital and on my own behalf, I, therefore, call upon all of you present and everyone everywhere in this world to join us in action for the realization of this grand dream. I humbly ask you to join us by:
1.  buying our shares so that you own the dream, its properties and benefits,
2.  referring clients, potential investors, medical professionals and other people to Glogoh.
3.  Encouraging, supporting, educating, guiding and augmenting our efforts. Not otherwise.
</p>
      </div>
      <div class="col-md-5 col-sm-6">
       <img class="img-responsive" src="images/welcome.jpg" alt="welcome medix">
      </div>
    </div>
  </div>
</section> 



<!-- Specialists -->
<section id="specialists" class="bg_grey padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      <h2 class="heading">Meet Our Specialists</h2>
      <hr class="heading_space">
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="slider_wrap">
          <div id="our-specialist" class="owl-carousel">
            <div class="item">
              <div class="specialist_wrap">
               <img src="images/our-specialist1.jpg" alt="Docotor">
               <h3>Dr. Andrew Bert</h3>
               <small>Outpatient Surgery</small>
               Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
              </div>
            </div>
            <div class="item">
              <div class="specialist_wrap">
               <img src="images/our-specialist2.jpg" alt="Docotor">
               <h3>Dr. Mecan smith</h3>
               <small>Heart Specialist</small>
               Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
              </div>
            </div>
            <div class="item">
              <div class="specialist_wrap">
               <img src="images/our-specialist3.jpg" alt="Docotor">
               <h3>Dr. Jack Bravo</h3>
               <small>Cardiologist</small>
               <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
              </div>
            </div>
            <div class="item">
              <div class="specialist_wrap">
               <img src="images/our-specialist1.jpg" alt="Docotor">
               <h3>Dr. Andrew Berton</h3>
               <small>Outpatient Surgery</small>
               <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
              </div>
            </div>
            <div class="item">
              <div class="specialist_wrap">
               <img src="images/our-specialist2.jpg" alt="Docotor">
               <h3>Dr. Andrew Berton</h3>
               <small>Outpatient Surgery</small>
               <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
              </div>
            </div>
            <div class="item">
              <div class="specialist_wrap">
               <img src="images/our-specialist3.jpg" alt="Docotor">
               <h3>Dr. Andrew Berton</h3>
               <small>Outpatient Surgery</small>
               <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>




<!--Blue Section-->
<section class="bg_blue padding-half">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-6 white_content">
        <i class="icon-patient-bed"></i>
        <h3>Operation Theater</h3>
        <p> you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing onsectetur Graphic Power Ut eros.</p>
      </div>
      <div class="col-md-4 col-sm-6 white_content">
        <i class="icon-mortar-pestle"></i>
        <h3><a href="services.html">Operation Theater</a></h3>
        <p> you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing onsectetur Graphic Power Ut eros.</p>
      </div>
      <div class="col-md-4 col-sm-6 white_content">
        <i class="icon-ambulanc"></i>
        <h3><a href="services.html">Operation Theater</a></h3>
        <p> you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing onsectetur Graphic Power Ut eros.</p>
      </div>
      <div class="col-md-4 col-sm-6 white_content">
        <i class="icon-patient-bed"></i>
        <h3><a href="services.html">Operation Theater</a></h3>
        <p> you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing onsectetur Graphic Power Ut eros.</p>
      </div>
      <div class="col-md-4 col-sm-6 white_content">
        <i class="icon-mortar-pestle"></i>
        <h3><a href="services.html">Operation Theater</a></h3>
        <p> you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing onsectetur Graphic Power Ut eros.</p>
      </div>
      <div class="col-md-4 col-sm-6 white_content">
        <i class="icon-ambulanc"></i>
        <h3><a href="services.html">Operation Theater</a></h3>
        <p> you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing onsectetur Graphic Power Ut eros.</p>
      </div>
    </div>
  </div>
</section>



<!--Hospital Facilities-->
<section id="hospital" class="padding bg_grey">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="heading">Hospital Facilities</h2>
        <hr class="heading_space">
      </div>
    </div>
    
    <div class="row">
      <div class="col-md-12 grid_layout">
      <div class="row">
      <div class="zerogrid">
          <div class="wrap-container">
            <div class="wrap-content clearfix">
              <div class="col-2-5">
              <div class="col-full">
              <div class="wrap-col">
                  <div class="item-container"> 
                   <img src="images/grid-layout1.jpg" alt="work"/> 
                   <div class="overlay">
                     <div class="overlay-inner">
                       <a class="fancybox icon" data-fancybox-group="gallery" href="images/grid-layout1.jpg">
                        <i class="icon-eye6"></i>
                      </a>
                     </div>
                   </div>
                  </div>
                </div>
              </div>
              </div>
              <div class="col-3-5">
              <div class="col-1-2">
              <div class="wrap-col first">
                  <div class="item-container"> 
                   <img src="images/grid-layout2.jpg" alt="work"/>
                   <div class="overlay">
                     <div class="overlay-inner">
                       <a class="fancybox icon" data-fancybox-group="gallery" href="images/grid-layout2.jpg">
                        <i class="icon-eye6"></i>
                      </a>
                     </div>
                   </div> 
                  </div>
                </div>
              </div>
              <div class="col-1-2">
              <div class="wrap-col first">
                  <div class="item-container"> 
                   <img src="images/grid-layout3.jpg" alt="work"/> 
                   <div class="overlay">
                     <div class="overlay-inner">
                       <a class="fancybox icon" data-fancybox-group="gallery" href="images/grid-layout3.jpg">
                        <i class="icon-eye6"></i>
                      </a>
                     </div>
                   </div>
                   </div>
                </div>
              </div>
              <div class="col-1-2">
              <div class="wrap-col">
                  <div class="item-container"> 
                   <img src="images/grid-layout4.jpg" alt="work"/> 
                   <div class="overlay">
                      <div class="overlay-inner">
                       <a class="fancybox icon" data-fancybox-group="gallery" href="images/grid-layout4.jpg">
                        <i class="icon-eye6"></i>
                      </a>
                     </div>
                   </div>
                    </div>
                </div>
              </div>
              <div class="col-1-2">
              <div class="wrap-col">
                  <div class="item-container"> 
                   <img src="images/grid-layout5.jpg" alt="work"/> 
                   <div class="overlay">
                      <div class="overlay-inner">
                       <a class="fancybox icon" data-fancybox-group="gallery" href="images/grid-layout5.jpg">
                        <i class="icon-eye6"></i>
                      </a>
                     </div>
                    </div>
                   </div>
                 </div>
               </div>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- testinomial -->
<section id="testinomial" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      <div id="testinomial-slider" class="owl-carousel text-center">
        <div class="item">
          <h3>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram.</h3>
          <p>Rodney Stratton, <span>Heart Patient</span></p>
        </div>
        <div class="item">
          <h3>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. quam nunc putamus parum claram, Mirum est notare quam littera gothica.</h3>
          <p>Rodney Robert, <span>Kidney Patient</span></p>
        </div>
        <div class="item">
          <h3>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse.</h3>
          <p>Rodney Alzbeth, <span>Liver Patient</span></p>
        </div>
       </div>
      </div>
    </div>
  </div>
</section>
