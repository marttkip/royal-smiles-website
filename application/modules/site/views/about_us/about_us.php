<?php
$company_details = $company_details;

$mission = $company_details['mission'];
$vision = $company_details['vision'];
$core_values = $company_details['core_values'];



?>

<?php
  $about_query = $this->site_model->get_active_content_items('Our Company',1);
   $about_company = '';
  if($about_query->num_rows() == 1)
  {
    $x=0;
    foreach($about_query->result() as $row)
    {
      $about_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $image_about = base_url().'assets/images/posts/'.$row->post_image;
      $created_by = $row->created_by;
      $modified_by = $row->modified_by;
      $post_target = $row->post_target;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));
      $x++;
      if($x < 9)
      {
        $x = '0'.$x;
      }

      $gallery_rs = $this->site_model->get_post_gallery($post_id);

      $gallery = '';
      if($gallery_rs->num_rows() > 0)
      {
        foreach ($gallery_rs->result() as $key => $value) {
          # code...
          $post_gallery_image_name = base_url().'assets/images/posts/'.$value->post_gallery_image_name;
          $post_gallery_image_thumb = $value->post_gallery_image_thumb;

          $gallery .= ' <li class="col-print-6">
                              <div class="portfolio-thumb">
                                  <div class="gc_filter_cont_overlay_wrapper port_uper_div">
                                      <img src="'.$post_gallery_image_name.'" class="zoom image img-responsive" alt="service_img" style="width:auto;height:130px !important;"/>
                                      <div class="gc_filter_cont_overlay zoom_popup">
                                          <div class="gc_filter_text"><a href="'.$post_gallery_image_name.'"><i class="fa fa-plus"></i></a>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </li>';
        }
      }

    }
  }
  ?>


  <?php
  $testimonials_query = $this->site_model->get_active_items('Company Testimonials');
   $testimonials = '';
  if($testimonials_query->num_rows() == 1)
  {
    $x=0;
    foreach($testimonials_query->result() as $row)
    {
      $about_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $testimonials_image_about = base_url().'assets/images/posts/'.$row->post_image;
      $created_by = $row->created_by;
      $modified_by = $row->modified_by;
      $post_target = $row->post_target;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));
      $x++;

      $testimonials .= '<div class="col-sm-6 col-md-6">
                          <div class="testimonial-1">
                                    <div class="media">
                                      <img src="'.$testimonials_image_about.'" alt="" class="img-fluid" >
                                    </div>
                                    <div class="body">
                                      <p>'.$description.'. </p>
                                      <div class="title">'.$post_title.'</div>
                                      <div class="company">'.$post_target.'</div>
                                    </div>
                                </div>
                        </div>';
    }
  }
  ?>

  <?php 

$branches_rs = $this->site_model->get_all_branches();

$branches_list = '<br/>';
$count_branch = $branches_rs->num_rows();
if($branches_rs->num_rows() > 0)
{
  $count = 0;
    foreach ($branches_rs->result() as $key => $value) {
        # code...
        $branch_name = $value->branch_name;
        $branch_email = $value->branch_email;
        $branch_phone = $value->branch_phone;
        $branch_address = $value->branch_address;
        // $branch_postal_code = $value->branch_postal_code;
        $branch_location = $value->branch_location;
        $branch_building = $value->branch_building;
        $branch_floor = $value->branch_floor;
        $location_link = $value->location_link;
        $count ++;

        
        $branches_list .= '<strong>'.$branch_location.'</strong> : '.$branch_building;
        if($count < $count_branch AND $count == $count_branch-1 )
        {
          $branches_list .='<br/>';
        }
        else if($count < $count_branch AND $count != $count_branch-1 )
        {
          $branches_list .='<br/>';
        }
    }
}
?>
 

 <div class="page-content">
        <!-- inner page banner -->
        <div class="dez-bnr-inr overlay-black-middle" style="background-image:url(<?php echo base_url().'assets/themes/theme/'?>images/banner/bnr1.jpg);">
            <div class="container">
                <div class="dez-bnr-inr-entry">
                    <h1 class="text-white">About us</h1>
                </div>
            </div>
        </div>
        <!-- inner page banner END -->
        <!-- Breadcrumb row -->
        <div class="breadcrumb-row">
            <div class="container">
                <ul class="list-inline">
                    <li><a href="<?php echo site_url().'home';?>">Home</a></li>
                    <li>About us</li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumb row END -->
        <!-- contact area -->
        <div class="clearfix">
            <!-- Our Awesome Services -->
            <div class="section-full bg-white content-inner">
                <div class="container">
                    <div class="section-content">
                        <div class="row">
                            <div class="col-lg-12 text-center section-head">
                            <h3 class="h3">About Royal Smiles Dental Clinic</h3>
                            <div class="dez-separator bg-primary"></div>
                                <div class="clear"></div>
                                <p>Royal Smiles Clinic is a modern dental clinic, specialised in advanced diagnostics and treatment of dental disorders. We guarantee comprehensive diagnostics and offer various forms of dental care, surgical procedures, and cosmetic dental services.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="icon-bx-wraper bx-style-2 m-b30 p-a20 left m-l50">
                                    <div class="icon-bx-md radius bg-primary"><span class="icon-cell text-white"><i class="fa fa-user-md" aria-hidden="true"></i></span></div>
                                    <div class="icon-content p-l40">
                                        <h5 class="dez-tilte">We Have Experienced Dentists</h5>
                                        <div class="dez-separator-outer ">
                                            <div class="dez-separator bg-primary style-liner"></div>
                                        </div>
                                        <p>We have a team of qualified dentists with a wide range of specialization in different areas of dentistry & several years of experience. </p>
                                    </div>
                                </div>
                                <div class="icon-bx-wraper bx-style-2 m-b30 p-a20 right m-r50">
                                    <div class="icon-bx-md radius bg-primary"> <span class="icon-cell text-white"><i class="fa fa-users" aria-hidden="true"></i></span> </div>
                                    <div class="icon-content p-r40">
                                        <h5 class="dez-tilte ">Well Qualified Staff</h5>
                                        <div class="dez-separator-outer ">
                                            <div class="dez-separator bg-primary style-liner"></div>
                                        </div>
                                        <p>Our staff undergo continuous training & mentorship to be able to offer highly professional services. </p>
                                    </div>
                                </div>
                                <div class="icon-bx-wraper bx-style-2 m-b30 p-a20 left m-l50">
                                    <div class="icon-bx-md radius bg-primary"> <span class="icon-cell text-white"><i class="fa fa-wheelchair" aria-hidden="true"></i></span> </div>
                                    <div class="icon-content p-l40">
                                        <h5 class="dez-tilte ">Accessibility</h5>
                                        <div class="dez-separator-outer ">
                                            <div class="dez-separator bg-primary style-liner"></div>
                                        </div>
                                        <p>All our dental clinics are easily accessible via public  & private transport as well as by people living with Disabilities. We got ample free parking in all our branches</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6  col-sm-12 text-center d-md-none d-lg-block">
                                <div class="worker"> <img src="<?php echo base_url().'assets/themes/theme/'?>images/service-image.png" alt=""/> </div>
                            </div>
                            <div class="col-lg-4 col-md-6  col-sm-12">
                                <div class="icon-bx-wraper bx-style-2 m-b30 p-a20 left m-l50">
                                    <div class="icon-bx-md radius bg-primary"> <span class="icon-cell text-white"><i class="fa fa-map-marker" aria-hidden="true"></i></span> </div>
                                    <div class="icon-content p-l40">
                                        <h5 class="dez-tilte ">We Have  <?php echo $branches_rs->num_rows();?> Branches</h5>
                                        <div class="dez-separator-outer ">
                                            <div class="dez-separator bg-primary style-liner"></div>
                                        </div>
                                        <p>With branches in <?php echo $branches_list?> our services have been brought near to you. </p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">

                    <div class="p-a50 bg-gray clearfix m-b0"> 
                      <div class="row">
                        <div class="col-md-2">
                        </div>
                         <div class="col-md-4">
                             <h2 style="width: 100%">
                             <a href="#" id="quik-search-btn" type="button" class="site-button col-md-12"> MISSION</a>
                             </h2>
                            <p><?php echo $mission?></p>
                        </div>
                         <div class="col-md-4">
                             <h2 style="width: 100%">
                             <a href="#" id="quik-search-btn" type="button" class="site-button col-md-12"> VISION</a>
                             </h2>
                            <p><?php echo $vision?></p>
                        </div>
                        <div class="col-md-2">
                        </div>
                      </div>
                       
                       
                    </div>
                </div>
            <!-- Our Awesome Services END -->
            <!-- Company Stats  -->
            <div class="section-full bg-img-fix content-inner overlay-black-dark our-projects-galery" style="background-image:url(images/background/bg3.jpg);">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-6 col-6 mb-4">
                            <div class="icon-bx-wraper center text-white">
                                <div class="icon-xl m-b20"> <span class="icon-cell"> <i class="fa fa-group"></i></span> </div>
                                <div class="m-t10">
                                    <div class="dez-separator bg-primary"></div>
                                </div>
                                <div class="icon-content">
                                    <h3 class="dez-tilte text-uppercase h3"> <span class="counter">15000</span> + </h3>
                                    <h6>Clients</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-6 mb-4">
                            <div class="icon-bx-wraper center text-white">
                                <div class="icon-xl m-b20"> <span class="icon-cell"> <i class="fa fa-user-md"></i></span> </div>
                                <div class="m-t10">
                                    <div class="dez-separator bg-primary"></div>
                                </div>
                                <div class="icon-content">
                                    <h3 class="dez-tilte text-uppercase h3"> <span class="counter">30</span> + </h3>
                                    <h6>Qualified staffs</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-6 mb-4">
                            <div class="icon-bx-wraper center text-white">
                                <div class="icon-xl m-b20"> <span class="icon-cell"> <i class="fa fa-map-marker"></i></span> </div>
                                <div class="m-t10">
                                    <div class="dez-separator bg-primary"></div>
                                </div>
                                <div class="icon-content">
                                    <h3 class="dez-tilte text-uppercase h3"> <span class="counter"><?php echo $branches_rs->num_rows();?></span> + </h3>
                                    <h6>branches country wide</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                            <div class="icon-bx-wraper center text-white">
                                <div class="icon-xl m-b20"> <span class="icon-cell"> <i class="fa fa-trophy"></i></span> </div>
                                <div class="m-t10">
                                    <div class="dez-separator bg-primary"></div>
                                </div>
                                <div class="icon-content">
                                    <h3 class="dez-tilte text-uppercase h3"> <span class="counter">4</span> + </h3>
                                    <h6>Awards of Association</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Company Stats END -->


            <!-- Team member -->
          
            <!-- Team member -->
            <!-- Testimonials -->
<!--             <div class="section-full bg-img-fix content-inner overlay-black-dark testimonials" style="background-image:url(images/background/bg3.jpg);">
                <div class="container">
                    <div class="section-head text-center text-white">
                        <h3 class="h3"> Testimonials</h3>
                        <div class="dez-separator bg-primary"></div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry has been the industry's standard dummy text ever since the been when an unknown printer.</p>
                    </div>
                    <div class="section-content">
                        <div class="testimonial-five owl-carousel owl-none">
                            <div class="item">
                                <div class="testimonial-6">
                                    <div class="testimonial-text bg-white quote-left quote-right">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an printer took a galley of type and scrambled it to make. </p>
                                    </div>
                                    <div class="testimonial-detail clearfix bg-primary text-white">
                                        <h4 class="testimonial-name m-tb0">David Matin</h4>
                                        <span class="testimonial-position">Student</span>
                                        <div class="testimonial-pic radius"><img src="images/testimonials/pic1.jpg" alt="" width="100" height="100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial-6">
                                    <div class="testimonial-text bg-white quote-left quote-right">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an printer took a galley of type and scrambled it to make. </p>
                                    </div>
                                    <div class="testimonial-detail clearfix bg-primary text-white">
                                        <h4 class="testimonial-name m-tb0">David Matin</h4>
                                        <span class="testimonial-position">Student</span>
                                        <div class="testimonial-pic radius"><img src="images/testimonials/pic2.jpg" alt="" width="100" height="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- Testimonials End -->
        </div>
        <!-- contact area END -->
    </div>

  

       <?php //echo $this->load->view("site/why_choose_us", '');?>  
    <!-- top service wrapper end-->
      <?php //echo $this->load->view("site/team_list", '');?>  

     <?php //echo $this->load->view("site/our_partners", '');?>  
    <!--news wrapper start-->
    

     <?php //echo $this->load->view("site/tag_line", '');?>  
      <?php echo $this->load->view("site/our_partners", '');?>  