<?php

		$result = '';
		
		//if users exist display them
	
		if ($query->num_rows() > 0)
		{	
			//get all administrators
			$administrators = $this->users_model->get_all_administrators();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			$count = 0;
			foreach ($query->result() as $row)
			{
				$post_id = $row->post_id;
				$blog_category_name = $row->blog_category_name;
				$blog_category_id = $row->blog_category_id;
				$post_title = $row->post_title;
				$web_name = $this->site_model->create_web_name($post_title);
				$post_status = $row->post_status;
				$post_views = $row->post_views;
				$image = base_url().'assets/images/posts/'.$row->post_image;
				$created_by = $row->created_by;
				$modified_by = $row->modified_by;
				$comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
				$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
				$description = $row->post_content;
				$mini_desc = strip_tags(implode(' ', array_slice(explode(' ', $description), 0, 50)));
				$created = $row->created;
				$day = date('j',strtotime($created));
				$month = date('M Y',strtotime($created));
				$created_on = date('jS M Y H:i a',strtotime($row->created));
				 $personnel_fname = $row->personnel_fname;

			    if(empty($personnel_fname))
			    {
			        $personnel_fname = 'Admin';
			    }
				
				$categories = '';
				
				//get all administrators
					// $administrators = $this->users_model->get_all_administrators();
					// if ($administrators->num_rows() > 0)
					// {
					// 	$admins = $administrators->result();
						
					// 	if($admins != NULL)
					// 	{
					// 		foreach($admins as $adm)
					// 		{
					// 			$user_id = $adm->user_id;
								
					// 			if($user_id == $created_by)
					// 			{
					// 				$created_by = $adm->first_name;
					// 			}
					// 		}
					// 	}
					// }
					
					// else
					// {
					// 	$admins = NULL;
					// }
				
					foreach($categories_query->result() as $res)
					{
						// $count++;
						$category_name = $res->blog_category_name;
						$category_id = $res->blog_category_id;
						
						if($count == $categories_query->num_rows())
						{
							$categories .= '<a href="'.site_url().'blog/category/'.$category_id.'" title="View all posts in '.$category_name.'" rel="category tag">'.$category_name.'</a>';
						}
						
						else
						{
							$categories .= '<a href="'.site_url().'blog/category/'.$category_id.'" title="View all posts in '.$category_name.'" rel="category tag">'.$category_name.'</a>, ';
						}
					}
					$comments_query = $this->blog_model->get_post_comments($post_id);
					//comments
					$comments = 'No Comments';
					$total_comments = $comments_query->num_rows();
					if($total_comments == 1)
					{
						$title = 'comment';
					}
					else
					{
						$title = 'comments';
					}
					
					if($comments_query->num_rows() > 0)
					{
						$comments = '';
						foreach ($comments_query->result() as $row)
						{
							$post_comment_user = $row->post_comment_user;
							$post_comment_description = $row->post_comment_description;
							$date = date('jS M Y H:i a',strtotime($row->comment_created));
							
							$comments .= 
							'
								<div class="user_comment">
									<h5>'.$post_comment_user.' - '.$date.'</h5>
									<p>'.$post_comment_description.'</p>
								</div>
							';
						}
					}
				// var_dump($count % 3);die();
				if ($count == 0) {
					// var_dump($count);die();
					$result .= '<div class="causes-wrap row">';
				}
				$result .= '

							<div class="col-md-4">
                                <div class="blog-post blog-grid date-style-2">
                                    <div class=""> <a href="'.site_url().'blog/view-single/'.$web_name.'"><img src="'.$image.'" alt="" style="height:auto !important;width:100% !important;"></a> </div>
                                    <div class="dez-post-info">
                                        <div class="dez-post-title ">
                                            <h3 class="post-title"><a href="'.site_url().'blog/view-single/'.$web_name.'">'.$post_title.'</a></h3>
                                        </div>
                                        <div class="dez-post-meta ">
                                            <ul>
                                                <li class="post-date"> <i class="fa fa-calendar"></i><strong>'.$created_on.'</strong> </li>
                                                <li class="post-author"><i class="fa fa-user"></i>By <a href="#">'.$personnel_fname.'</a> </li>
                                            </ul>
                                        </div>
                                        <div class="dez-post-text">
                                            <p>'.$mini_desc.'</p>
                                        </div>
                                        <div class="dez-post-readmore"> <a href="'.site_url().'blog/view-single/'.$web_name.'" title="'.$post_title.'" rel="bookmark" class="site-button-link">READ MORE<i class="fa fa-angle-double-right"></i></a> </div>
                                    </div>
                                </div>
                            </div>
		         
		            ';
		            $count++;
		            if ($count % 3 == 0) {

						$result .= '</div>
									<div class="clearfix"></div>
									<div class="causes-wrap row">';
					}


		        }
			}
			else
			{
				$result = "";
			}
           
          ?>     
 <!-- Content -->
<div class="page-content">
    <!-- inner page banner -->
    <div class="dez-bnr-inr overlay-black-middle" style="background-image:url(<?php echo base_url().'assets/themes/theme/'?>images/banner/bnr1.jpg);">
        <div class="container">
            <div class="dez-bnr-inr-entry">
                <h1 class="text-white">What's new</h1>
            </div>
        </div>
    </div>
    <!-- inner page banner END -->
    <!-- Breadcrumb row -->
    <div class="breadcrumb-row">
        <div class="container">
            <ul class="list-inline">
                <li><a href="<?php echo site_url().'home'?>">Home</a></li>
                <li>What's new</li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumb row END -->
    <div class="content-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 p-lr0">
					<!-- blog grid -->
					<div id="masonry" class="dez-blog-grid-3">
						<?php echo $result;?>
					
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- blog grid END -->
	<!-- Pagination -->
	<div class="row">
		<div class="pagination-bx col-lg-12 clearfix">
			<ul class="pagination">
				
			</ul>
		</div>
	</div>
	<!-- Pagination END -->
</div>

 <?php echo $this->load->view("site/our_partners", '');?>  