<?php

$post_id = $row->post_id;
$blog_category_name = $row->blog_category_name;
$blog_category_id = $row->blog_category_id;
$post_title = $row->post_title;
$post_video = $row->post_video;
$intro = $row->post_target;
$post_status = $row->post_status;
$post_views = $row->post_views;
$image = base_url().'assets/images/posts/'.$row->post_image;
$created_by = $row->created_by;
$modified_by = $row->modified_by;
$comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
$body = $row->post_content;
$mini_desc = implode(' ', array_slice(explode(' ', $body), 0, 50));
$created = $row->created;
$day = date('j',strtotime($created));
$month = date('M Y',strtotime($created));
$transdate = date('jS M Y H:i a',strtotime($row->created));

$categories = '';
$count = 0;
//get all administrators
	$administrators = $this->users_model->get_all_administrators();
	if ($administrators->num_rows() > 0)
	{
		$admins = $administrators->result();
		
		if($admins != NULL)
		{
			foreach($admins as $adm)
			{
				$user_id = $adm->personnel_id;
				
				if($user_id == $created_by)
				{
					$created_by = $adm->personnel_fname.' '.$adm->personnel_onames;
				}
			}
		}
	}
	
	else
	{
		$admins = NULL;
		$created_by = '';
	}
$gallery_rs = $this->site_model->get_post_gallery($post_id);

$gallery = '';
if($gallery_rs->num_rows() > 0)
{
	foreach ($gallery_rs->result() as $key => $value) {
	  # code...
	  $post_gallery_image_name = base_url().'assets/images/posts/'.$value->post_gallery_image_name;
	  $post_gallery_image_thumb = $value->post_gallery_image_thumb;

	  $gallery .= ' <div class="col-xl4 col-lg-4 col-md-4 col-sm-12 col-12">
	                    <div class="blog_about">
	                        <div class="blog_img blog_post_img">
	                            <figure>
	                                <img src="'.$post_gallery_image_name.'" alt="img" class="img-responsive">
	                            </figure>
	                        </div>
	                    </div>
	                </div>';
	}
}

	foreach($categories_query->result() as $res)
	{
		$count++;
		$category_name = $res->blog_category_name;
		$category_id = $res->blog_category_id;
		
		if($count == $categories_query->num_rows())
		{
			$categories .= '<a href="'.site_url().'blog/category/'.$category_id.'" title="View all posts in '.$category_name.'" rel="category tag">'.$category_name.'</a>';
		}
		
		else
		{
			$categories .= '<a href="'.site_url().'blog/category/'.$category_id.'" title="View all posts in '.$category_name.'" rel="category tag">'.$category_name.'</a>, ';
		}
	}
	$comments_query = $this->blog_model->get_post_comments($post_id);
	//comments
	$comments = 'No Comments';
	$total_comments = $comments_query->num_rows();
	if($total_comments == 1)
	{
		$title = 'comment';
	}
	else
	{
		$title = 'comments';
	}
	
	if($comments_query->num_rows() > 0)
	{
		$comments = '';
		foreach ($comments_query->result() as $row)
		{
			$post_comment_user = $row->post_comment_user;
			$post_comment_description = $row->post_comment_description;
			$date = date('jS M Y H:i a',strtotime($row->comment_created));
			
			$comments .= 
			'
				<div class="user_comment">
					<h5>'.$post_comment_user.' - '.$date.'</h5>
					<p>'.$post_comment_description.'</p>
				</div>
			';
		}
	}
	



?>



 <!-- Content -->
    <div class="page-content">
        <!-- inner page banner -->
        <div class="dez-bnr-inr overlay-black-middle" style="background-image:url(<?php echo base_url().'assets/themes/theme/'?>images/banner/bnr1.jpg);">
            <div class="container">
                <div class="dez-bnr-inr-entry">
                    <h1 class="text-white"><?php echo $post_title;?></h1>
                </div>
            </div>
        </div>
        <!-- inner page banner END -->
        <!-- Breadcrumb row -->
        <div class="breadcrumb-row">
            <div class="container">
                <ul class="list-inline">
                    <li><a href="<?php echo site_url().'home'?>">Home</a></li>
                    <li><a href="<?php echo site_url().'whats-new'?>">Blog</a></li>
                    <li><?php echo $post_title;?></li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumb row END -->
        <div class="content-area">
            <div class="container">
                <div class="row">
                	<div class="col-lg-9 mb-4">
                    <!-- Left part start -->

		                <!-- blog start -->
		                <div class="blog-post blog-single">
		                    <div class="dez-post-title ">
		                        <h3 class="post-title"><a href="#"><?php echo $title;?></a></h3>
		                    </div>
		                    <div class="dez-post-meta m-b20">
		                        <ul>
		                            <li class="post-date"> <i class="fa fa-calendar"></i><strong><?php echo $transdate;?></strong> </li>
		                            <li class="post-author"><i class="fa fa-user"></i>By <a href="#"><?php echo $created_by?></a> </li>	                     
		                        </ul>
		                    </div>
		                    <div class="dez-post-media dez-img-effect zoom-slow"> <a href="#"><img src="<?php echo $image;?>" alt="" style="height:500px;"></a> </div>
		                    <div class="dez-post-text">
		                    	<blockquote><?php echo $intro;?></blockquote>

		                        <p><?php echo $body;?></p>
		                        <div>
		                        	<!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/PbvmbrMq52A" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
		                        	<iframe width="100%" height="315" src="<?php echo $post_video?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		                        </div>
		                        </div>


								<div class="fb-like offset-top-45" data-href="" data-layout="standard" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
								<br/>
						
								<div class="fb-comments" data-href="" data-numposts="10" data-width="100%" data-colorscheme="light"></div>
		                </div>
					</div>


                    <!-- Side bar start -->
                    <div class="col-lg-3">
                        <aside  class="side-bar">
                            <div class="widget recent-posts-entry">
                                <h4 class="widget-title">Recent Posts</h4>

                                <?php
                                $about_query = $this->site_model->get_active_items('Company Blog',5);
								 $blog_list = '';

								 // var_dump($about_query);die();
								if($about_query->num_rows() > 0)
								{
								  $x=0;
								  foreach($about_query->result() as $row)
								  {
								    $about_title = $row->post_title;
								    $post_id = $row->post_id;
								    $blog_category_name = $row->blog_category_name;
								    $blog_category_id = $row->blog_category_id;
								    $post_title = $row->post_title;
								    $web_name = $this->site_model->create_web_name($post_title);
								    $post_status = $row->post_status;
								    $post_views = $row->post_views;
								    $image_about = base_url().'assets/images/posts/'.$row->post_image;
								    $created_by = $row->created_by;
								    $modified_by = $row->modified_by;
								    $post_target = $row->post_target;
								    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
								    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
								    $description = $row->post_content;
								    $mini_desc = strip_tags(implode(' ', array_slice(explode(' ', $description), 0, 50)));
								    $created = $row->created;
								    $day = date('j',strtotime($created));
								    $month = date('M',strtotime($created));
								    $year = date('Y',strtotime($created));
								    $created_on = date('jS M Y',strtotime($row->created));
								    $x++;
								    if($x < 9)
								    {
								      $x = '0'.$x;
								    }
								    $blog_list .= '
								    				<div class="widget-post-bx">
					                                    <div class="widget-post clearfix">
					                                        <div class="dez-post-media"> <img src="'.$image_about.'" width="200" height="auto" alt=""> </div>
					                                        <div class="dez-post-info">
					                                            <div class="dez-post-header">
					                                                <h6 class="post-title"><a href="'.site_url().'blog/view-single/'.$web_name.'">'.$post_title.'</a></h6>
					                                            </div>
					                                            <div class="dez-post-meta">
					                                                <ul>
					                                                    <li class="post-author">By <a href="#">Admin</a></li>
					                                                </ul>
					                                            </div>
					                                        </div>
					                                    </div>
					                                </div>
								    					

								                               
											                ';
								  }
								}

								echo $blog_list;
	             				?>



	                            </div>
	                        </aside>
	                    </div>
	                    <!-- Side bar END -->



</div>
                <!-- blog END -->
            </div>
        </div>
    </div>
    <!-- Content END-->
