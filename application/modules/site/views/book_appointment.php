  	<?php
  	$branches_rs = $this->site_model->get_all_branches();

    $branches_list = '';
    if($branches_rs->num_rows() > 0)
    {
        foreach ($branches_rs->result() as $key => $value) {
            # code...
            $branch_name = $value->branch_name;
            $branch_email = $value->branch_email;
            $branch_id = $value->branch_id;
            $branch_phone = $value->branch_phone;
            $branch_address = $value->branch_address;
            // $branch_postal_code = $value->branch_postal_code;
            $branch_location = $value->branch_location;
            $branch_building = $value->branch_building;
            $branch_floor = $value->branch_floor;
            $branch_code = $value->branch_code;
            $location_link = $value->location_link;
            $branches_list .= '
            					<option value="'.$branch_code.'#'.$branch_id.'#'.$branch_email.'">'.$branch_name.'</option>
                            ';
        }
    }
  	?>
  <!-- Content -->
    <div class="page-content">
        <!-- Breadcrumb row -->
        <div class="breadcrumb-row">
            <div class="container">
                <ul class="list-inline">
                    <li><a href="<?php echo site_url().'home'?>">Home</a></li>
                    <li>Contact us</li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumb row END -->
        <!-- contact area -->
        <div class="section-full content-inner-1 bg-white contact-style-1">
			<div class="container">
                <div class="row">
                    <!-- Left part start -->
                    <div class="col-lg-12">
                        <div class="p-a30 bg-gray clearfix m-b30 ">
							<h2>Book an appointment</h2>

							<?php
							$error = $this->session->userdata('error_message');
							$success = $this->session->userdata('success_message');
							
							if(!empty($error))
							{
								echo '<div class="alert alert-danger">'.$error.'</div>';
								$this->session->unset_userdata('error_message');
							}
							
							if(!empty($success))
							{
								echo '<div class="alert alert-success">'.$success.'</div>';
								$this->session->unset_userdata('success_message');
							}
							?>
							
                           <!--  <form data-form-output="form-output-global" data-form-type="contact" method="post" action="book_appointment.php"> -->
                            <?php echo form_open($this->uri->uri_string(), array("id" => "contact-form","data-form-output" => "form-output-global", "data-form-type"=>"contact" ));?>
                              <div class="range">
                                <div class="cell-md-6">
                                  <div class="form-group">
                                    <input id="contact-name" type="text" name="name" data-constraints="@Required" placeholder="Enter your Name" class="form-control" required="required">
                                  </div>
                                </div>
                                <div class="cell-md-6 offset-top-45 offset-sm-top-0">
                                  <div class="form-group">
                                    <input id="contact-phone" type="text" name="phone" data-constraints="@Required @Numeric" placeholder="Enter your Phone number" class="form-control" required="required">
                                  </div>
                                </div>
                                <div class="cell-md-6 offset-top-45 offset-sm-top-0">
                                  <div class="form-group">
                                    <input id="contact-email-1" type="email" name="email" data-constraints="@Email @Required" placeholder="Enter your Email" class="form-control" required="required">
                                  </div>
                                </div>
                                <div class="cell-md-6 offset-top-45 offset-sm-top-0 row">
                                  <div class="form-group col-md-6">
                                    <label>Time</label>
                                    <input id="contact-email-1 " type="time" name="time" data-constraints="@saa @Required" data-format=" hh:mm:ss" class="form-control" placeholder="Time" required="required">
                                  </div>                	
                                  <div class="form-group col-md-6">
                                    <label>Day</label>
                                    <input id="contact-email-1 day1" type="date" name="date" data-constraints="@day @Required" data-format="dd/MM/yyyy hh:mm:ss" placeholder="Select date" class="form-control" required="required">
                                  </div>

                                </div>
                                <div class="cell-md-6 offset-top-45 offset-sm-top-0">
                                	<div class="form-group">
                                		<label>Have you visited us at any of our branches before? </label>
                                		<select  id="sel1" name="visit_status" required="required">
                                          <option value="">Select option</option>
                                          <option value="0">Yes</option>
                                          <option value="1">No</option>
                                         
                                       </select>
                                	</div>
                                </div>
                                <div class="cell-md-6 offset-top-45 offset-sm-top-0">
                                  <div class="form-group">
                                  	  <label>Branch</label>
                                      <select  id="sel1" name="branch" required="required">
                                          <option value="">Select Branch</option>
                                          <?php echo $branches_list;?>
                                       </select>
                                  </div>
                                </div>
                                <div class="cell-xs-12 offset-top-45">
                                  <div class="form-group">
                                    <textarea id="contact-message" name="remarks" data-constraints="@Required" placeholder="Enter Inquiry" class="form-control"></textarea>
                                  </div>
                                </div>

                                <div class="col-md-12">
                                      <div class="g-recaptcha" data-sitekey="6LcsA4knAAAAAGgvaRWFXhc-GGJUPjjyMieeOJu2"></div>
                                  </div>
                              </div>
                              <div class="text-center offset-top-30">
                                <button type="submit" class="site-button btn ">Book an appointment</button>
                              </div>
                            </form>
                        </div>
                    </div>
                    <!-- Left part END -->

                </div>

            </div>
        </div>
        <!-- contact area  END -->
    </div>
    <!-- Content END-->
