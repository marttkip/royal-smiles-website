<?php
  $about_query = $this->site_model->get_active_items('Organization Causes');
   $causes_list = '';
  if($about_query->num_rows() == 1)
  {
    $x=0;
    foreach($about_query->result() as $row)
    {
      $about_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $image_about = base_url().'assets/images/posts/'.$row->post_image;
      $created_by = $row->created_by;
      $modified_by = $row->modified_by;
      $post_target = $row->post_target;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));
      $x++;
      if($x < 9)
      {
        $x = '0'.$x;
      }

      $causes_list .= '<div class="col-sm-4 col-md-4">
				            <div class="box-fundraising">
			              		<div class="media">
			                		<img src="'.$image_about.'" alt="">
			              		</div>
			              		<div class="body-content">
			              			<p class="title"><a href="cause-single.html">'.strtoupper($post_title).'</a></p>
			              			<div class="text">'.$mini_desc.'</div>
			              			<div class="progress-fundraising">
				              			<div class="total">'.$post_target.' <span>to go</span></div>
			              			  	<div class="persen">80%</div>
				              			<div class="progress">
										  <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
									</div>
			              		</div>
				            </div>
				        </div>';
    }
  }
  ?>
<!-- BANNER -->
	<div class="section banner-page" data-background="<?php echo base_url().'assets/themes/ngoo/'?>images/banner-single.jpg">
		<div class="content-wrap pos-relative">
			<div class="d-flex justify-content-center bd-highlight mb-3">
				<div class="title-page">Our Causes</div>
			</div>
			<div class="d-flex justify-content-center bd-highlight mb-3">
			    <nav aria-label="breadcrumb">
				  <ol class="breadcrumb ">
				    <li class="breadcrumb-item"><a href="<?php echo site_url().'home'?>">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Our Causes</li>
				  </ol>
				</nav>
		  	</div>
		</div>
	</div>

	<!-- HOW TO HELP US -->
	<div class="section">
		<div class="content-wrap">
			<div class="container">

				<div class="row">

					<div class="rs-nav-tabs">
						
						<div class="tab-content" id="pills-tabContent">
						  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
						  	<div class="col-md-12">
						  		<div class="row">
						  				<?php echo $causes_list?>

						  		</div>
						  	</div>

						  </div>
						  <!-- END TAB 1 CONTENT -->

						 
						  <!-- END TAB 2 CONTENT -->
						</div>
					</div>


					
				</div>

			</div>
		</div>
	</div>
	<?php echo $this->load->view("site/bottom_flier", '');?>  