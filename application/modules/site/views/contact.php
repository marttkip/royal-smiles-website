<?php 
	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$phone = $contacts['phone'];
		$facebook = $contacts['facebook'];
		$twitter = $contacts['twitter'];
		$linkedin = $contacts['linkedin'];
        $instagram = $contacts['instagram'];
		$logo = $contacts['logo'];
		$company_name = $contacts['company_name'];
		$address = $contacts['address'];
		$city = $contacts['city'];
		$post_code = $contacts['post_code'];
		$building = $contacts['building'];
		$floor = $contacts['floor'];
		$location = $contacts['location'];
		$working_weekend = $contacts['working_weekend'];
		$working_weekday = $contacts['working_weekday'];
		
		// if(!empty($email))
		// {
		// 	$mail = '<div class="top-number"><p><i class="fa fa-envelope-o"></i> '.$email.'</p></div>';
		// }
		
		// if(!empty($facebook))
		// {
		// 	$facebook = '<a href="'.$facebook.'" target="_blank">Facebook</a>';
		// }
		
		// if(!empty($twitter))
		// {
		// 	$twitter = '<a href="'.$twitter.'" target="_blank">Twitter</a>';
		// }
		
		// if(!empty($linkedin))
		// {
		// 	$linkedin = '<a href="'.$linkedin.'" target="_blank"></a>';
		// }
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
        $instagram = '';
		$logo = '';
		$company_name = '';
	}

   $branches_rs = $this->site_model->get_all_branches();

    $branches_list = '';
    if($branches_rs->num_rows() > 0)
    {
        foreach ($branches_rs->result() as $key => $value) {
            # code...
            $branch_name = $value->branch_name;
            $branch_email = $value->branch_email;
            $branch_phone = $value->branch_phone;
            $branch_address = $value->branch_address;
            // $branch_postal_code = $value->branch_postal_code;
            $branch_location = $value->branch_location;
            $branch_building = $value->branch_building;
            $branch_floor = $value->branch_floor;
            $location_link = $value->location_link;
            $branches_list .= '
                            <div class="col-md-3">
                                <i class="fa fa-map-marker"></i> <b>'.$branch_name.'</b><br />
                                <p> '.$branch_building.' '.$branch_floor.' '.$branch_location.'</p>
                                <p> '.$branch_phone.'</p>
                            </div>';
        }
    }

?>


<!-- Content -->
<div class="page-content">
    <!-- inner page banner -->
    <div class="dez-bnr-inr overlay-black-middle" style="background-image: url(<?php echo base_url().'assets/themes/theme/'?>images/background/bg4.jpg);">
        <div class="container">
            <div class="dez-bnr-inr-entry">
                <h1 class="text-white">Contact Us</h1>
            </div>
        </div>
    </div>
    <!-- inner page banner END -->
    <!-- Breadcrumb row -->
    <div class="breadcrumb-row">
        <div class="container">
            <ul class="list-inline">
                <li><a href="<?php echo site_url().'home'?>">Home</a></li>
                <li>Contact us</li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumb row END -->
    <!-- contact area -->
    <div class="section-full content-inner-1 bg-white contact-style-1">
        <div class="container">
            <div class="row">
                <!-- Left part start -->
                <div class="col-md-12">

                    <!-- <div class="p-a30 bg-gray clearfix m-b30"> -->
                        <div class="row">
                             <h2 style="width: 100%">
                             <a href="#" id="quik-search-btn" type="button" class="site-button col-md-12"> Talk to Us</a>
                             </h2>
                            <!-- <p>If you have any questions simply use the following contact details.</p> -->
                        </div>
                        <h2> WhatsApp ; <a href="https://wa.link/8zdvij" target="_BLANK" class=""> <i class="fa fa-whatsapp"></i>  0732685292</a></h2>
                         <h2>Email ; <a href="mailto:<?php echo $email?>" target="_BLANK" class=""> <i class="fa fa-envelope-o"></i>  <?php echo $email?></a></h2>
                    <!-- </div> -->
                </div>
                <!-- Left part END -->
                <!-- right part start -->
                <div class="col-md-12">
                        <!--  <div class="row">
                             <h2 style="width: 100%">
                             <a href="#" id="quik-search-btn" type="button" class="site-button col-md-12"> Quick Contacts</a>
                             </h2>
                            
                        </div>
                        <div class="row">
                            <?php echo $branches_list?>
                        </div> -->
                            
                        <!-- </ul> -->
                         <div class="m-t20">
                            <section id="header-strip">
                            <ul class="dez-social-icon border dez-social-icon-lg">
                                <li><a href="<?php echo $facebook?>" target="_BLANK" class="fa fa-facebook fb-btn"></a></li>
                                <li><a href="<?php echo $twitter?>" target="_BLANK" class="fa fa-twitter tw-btn"></a></li>
                                <li><a href="<?php echo $linkedin?>" target="_BLANK" class="fa fa-linkedin link-btn"></a></li>
                                <li><a href="<?php echo $instagram?>" target="_BLANK" class="fa fa-instagram pin-btn"></a></li>
                            </ul>
                        </section>
                        </div> 
                    <!-- </div> -->
                </div>
                <!-- right part END -->
            </div>
           
        </div>
    </div>
    <!-- contact area  END -->
</div>
<!-- Content END-->

 <?php echo $this->load->view("site/our_partners", '');?>  
