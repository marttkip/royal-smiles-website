<!--Page header & Title-->
<section id="page_header">
<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Contact Us</h2>
         <div class="page_link"><a href="<?php echo site_url().'home'?>">Home</a><span><i class="fa fa-long-arrow-right"></i>Contact Us</span></div>
      </div>
    </div>
  </div>
</div>  
</section>

<!--LOcation Map-->
<!--<section class="padding">-->
<!--<h3 class="hidden">hidden</h3>-->
<!--  <div class="container">-->
<!--    <div class="row">-->
<!--      <div class="col-md-12">-->
        <!--<div id="map"></div>-->
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2503.666215489201!2d35.27392654219571!3d0.5166274682129043!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x178101a45ebdf83d%3A0xdad47d19b8fc93c3!2s64+Oloo+St%2C+Eldoret!5e0!3m2!1sen!2ske!4v1536598189294" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
       
    
<!--        </div>-->
<!--      </div>-->
<!--    </div>-->
<!--  </div>-->
<!--</section>-->

<!--Contact Form & Address-->
<section class="padding bg_grey">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-8">
        <h2 class="heading">Get in Touch</h2>
        <hr class="heading_space">
        <form class="callus" onSubmit="return false"  id="contact_form">
          <div class="row">
          
           <div class="col-md-12">
                    <div id="result" class="text-center form-group"></div>
           </div>
          
            <div class="col-md-6">
              <div class="form-group">
                <input class="form-control" type="text" name="f_name" id="f_name"  placeholder="First Name" required />
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <input class="form-control" type="text" name="l_name" id="l_name"  placeholder="Last Name" required />
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <input class="form-control" type="email" name="email" id="email"  placeholder="Email Address" required />
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <input class="form-control" type="text" name="phone" id="phone" required placeholder="Phone No">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <textarea placeholder="Message" name="message" id="message"></textarea>
              </div>
              <div class="form-group">
                 <div class="btn-submit button3">
                <input type="submit" value="Submit Request" id="btn_contact_submit">
                
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="col-md-4 col-sm-4 medix">
        <h2 class="heading">MediX</h2>
        <hr class="heading_space"> 
        <p><strong>Phone:</strong> 1.800.555.6789</p>
        <p><strong>Email:</strong> <a href="mailto:support@medix.com">support@medix.com</a></p>
        <p><a href="contact.html#">Web: www.medix.com</a></p>
        <p><strong>Address:</strong> 12345 North Main Street, New York</p>
          <ul class="social_icon">
            <li class="black"><a href="contact.html#" class="facebook"><i class="icon-facebook5"></i></a></li>
            <li class="black"><a href="contact.html#" class="twitter"><i class="icon-twitter4"></i></a></li>
            <li class="black"><a href="contact.html#" class="google"><i class="icon-google"></i></a></li>
          </ul>
      </div>
    </div>
  </div>
</section>
