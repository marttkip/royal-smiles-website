<?php
    $gallery = $this->site_model->get_active_service_gallery_names();

  
    $gallery_items_names = '';
    if($gallery->num_rows() > 0)
    {   
        foreach($gallery->result() as $res_gallery)
        {
            $gallery_name = $res_gallery->gallery_name;
                    

                   
            $gallery_items_names .='<li><a href="#filter=.'.$gallery_name.'" title="'.$gallery_name.'">'.$gallery_name.'</a></li>';
        }
    }

    $gallery_div = $this->site_model->get_active_gallery();
    $gallery_items = '';
    if($gallery_div->num_rows() > 0)
    {   
        foreach($gallery_div->result() as $res_gallery_div)
        {
            $gallery_name = $res_gallery_div->gallery_name;
            $gallery_image_name = $res_gallery_div->gallery_image_name;
            $gallery_image_thumb = $res_gallery_div->gallery_image_thumb;

             $gallery_items .=
                                    '

                                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 portfolio-wrapper all  '.$gallery_name.'">
                                      <div class="portfolio-thumb">
                                          <div class="port_img_wrapper">
                                              <img src="'.$gallery_location.''.$gallery_image_name.'" alt="filter_img">
                                          </div>
                                          <div class="portfolio_icon_wrapper_iv zoom_popup">
                                              <a class="img-link" href="'.$gallery_location.''.$gallery_image_name.'"> <i class="fa fa-search"></i>
                                              </a>
                                          </div>

                                          <div class="portfolio-content">
                                              <div class="portfolio-info_iv">
                                                  <h3>'.$gallery_name.'</h3>
                                                  <p class="d-none d-sm-block"></p>
                                              </div>
                                          </div>

                                      </div>
                                  </div>
                                  
                                    
                                   
                                    ';      
        }
    }
?>


<!--med_tittle_section-->
    <div class="med_tittle_section">
        <div class="med_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="med_tittle_cont_wrapper">
                        <div class="med_tittle_cont">
                            <h1>gallery </h1>
                            <ol class="breadcrumb">
                                <li><a href="<?php echo site_url().'home'?>">Home</a>
                                </li>
                                <li>gallery</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- med_tittle_section End -->


  <!-- sdas -->

  <!-- portfolio-section start -->
    <section class="portfolio-area med_toppadder100">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <ul class="protfoli_filter gallerY_wrapper">
                        <li class="active" data-filter=".all"><a href="#"> All</a></li>
                        <?php echo $gallery_items_names;?>
                    </ul>
                </div>
                <div class="col-lg-12">
                    <div class="row portfoli_inner">
                        
                        <?php echo $gallery_items;?>
                    </div>
                    <!--/#gridWrapper-->
                </div>
                <!-- /.row -->
                <div class="text-center portfolio-btn  gallerY_btn med_bottompadder100">
                    <a href="gallery_4.html#" class="btn">Load More</a>
                </div>
            </div>
        </div>
        <!-- /.container -->
    </section>
    <!--/.portfolio-area-->
    <!-- portfolio-section end -->
    <!--news wrapper start-->
    <!--partner wrapper end-->
    <!--news wrapper start-->
    <?php echo $this->load->view("site/tag_line", '');?>  
    <!--news wrapper end-->