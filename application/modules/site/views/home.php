<?php
    $company_details = $this->site_model->get_contacts();
    
    if(count($company_details) > 0)
    {
        $email = $company_details['email'];
        $email2 = $company_details['email'];
        $facebook = $company_details['facebook'];
        $twitter = $company_details['twitter'];
        $linkedin = '';// $company_details['linkedin'];
        $logo = $company_details['logo'];
        $company_name = $company_details['company_name'];
        $phone = $company_details['phone']; 
        $mission = $company_details['mission'];     
    
    }
    else
    {
        $email = '';
        $facebook = '';
        $twitter = '';
        $linkedin = '';
        $logo = '';
        $company_name = '';
        $google = '';
        $mission = '';
    }


?>

<div class="page-content bg-white">
    <!-- Slider -->
        <div class="main-slider style-two default-banner">
            <div class="tp-banner-container">
                <div class="tp-banner" >
                    <div id="dz_rev_slider_4_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery36" data-source="gallery" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
                        <!-- START REVOLUTION SLIDER 5.3.0.2 fullwidth mode -->
                        <div id="slider_03" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.3.0.2">
                            <ul>


                                <!-- SLIDE  -->
                                <?php echo $this->load->view("home/slider", '');?> 
                                <!-- SLIDE  -->


                                
                            </ul>
                            <div class="tp-bannertimer tp-bottom bg-primary" style="height: 5px; "></div>
                        </div>
                    </div>
                    <!-- END REVOLUTION SLIDER -->
                </div>
            </div>
        </div>
        <!-- Slider END -->
        <!-- Get A Quote -->
        <div class="section-full bg-img-fix overlay-primary-dark content-inner-1 dez-support" style="background-image:url(images/background/bg4.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center text-white ">
                        <h2 class="m-b15 m-t0" style="font-family:ubuntu Condended;">Welcome to Royal Smiles Dental Clinic</h2>
                        <h3 class="m-t0 m-b20">Let us show you how easy and affordable it is to RESTORE YOUR SMILE</h3>
                        <h3>Our Dental clinic combines years of experience with new technologies.</h3>
                        <a href="<?php echo site_url().'book-appointment'?>" class="site-button white radius-sm">Book Appointment</a>
                    </div>
               </div>
            </div>
        </div>
        <!-- Get A Quote END -->
        <!-- About Company -->
        <div class="section-full content-inner bg-white">
            <div class="container">
                <div class="section-content">
                    <div class="row text-center">
                        
                        <?php echo $this->load->view("site/main_services", '');?>  
                        
                    </div>
                </div>
            </div>
        </div>

</div>
   
   
    <!--appoint wrapper end-->
    <!--choose wrapper start-->
  
    <!--choose wrapper end-->
    <!--team wrapper start-->
    <?php //echo $this->load->view("site/why_choose_us", '');?>  
   
    <!--team wrapper end-->
    <!--vedio wrapper start-->
    
    <!--vedio wrapper end-->
    <!--event wrapper start-->
   
    <!-- event wrapper end-->
    <!-- blog wrapper start-->
    <?php echo $this->load->view("site/recent_blogs", '');?>  
    <!-- blog wrapper end-->
    <!--testimonial wrapper start-->
    <?php //echo $this->load->view("site/testimonials", '');?>  
                           
                        
    <!-- testimonial wrapper end-->
    <!--partner wrapper start-->
    
   <?php echo $this->load->view("site/our_partners", '');?>  



                       
    <!--partner wrapper end-->
    <!--news wrapper start-->
    <?php //echo $this->load->view("site/tag_line", '');?>  
    <!--news wrapper end-->


