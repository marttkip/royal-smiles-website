<?php
 if($slides->num_rows() > 0)
{
  $slides_no = $slides->num_rows();
  // var_dump($slides_no);die();
  $x= 0;
  $y= 0;
  $z=100;
  $count=0;
  foreach($slides->result() as $cat => $value)
  {   

    $slideshow_id = $value->slideshow_id;
    $slideshow_status = $value->slideshow_status;
    $slideshow_name = $value->slideshow_name;
    $slideshow_description = $value->slideshow_description;
    $slideshow_image_name = $value->slideshow_image_name;
    $slideshow_link = $value->slideshow_link;
    $slideshow_button_text = $value->slideshow_button_text;
    $subtitle = $value->subtitle;
    $slideshow_thumb_name = 'thumbnail_'.$value->slideshow_image_name;
 

 	$med = explode(' ', $slideshow_name,2);

 	$first_name = $med[0];
 	$second_name = $med[1];

 
    $x+100;


 	if(!empty($slideshow_link))
 	{
 		$buttons = ' <ul>
                        <li data-animation-in="bounceInLeft" data-animation-out="animate-out bounceOutLeft"><a href="'.$slideshow_button_link.'">'.$slideshow_button_text.'</a></li>
                    </ul>';
	}
	else
	{
		$buttons = '';
	}
    if($y == 0)
    {
        $y = '';
    }
    // if($cou)
    $count++;

    // if ($count % 3 != 0) {
    //      $slide_value = 200;
    //      $item_value = 1;
    // }
    // else
    // {
    //     $slide_value = 100;
    //     $item_value = '';
    // }
    $slide_value = $count.'00';
    $item_value = '';
    // if($count == 1)
    // {
    //     $slide_value = $count.'00';
    //     $item_value = '';
    // }
    // else if($count == 2)
    // {
    //     $slide_value = $count.'00';
    //     $item_value = '';
    // }
    // else if($count == 3)
    // {
    //      $slide_value = $count.'00';
    //      $item_value = '';
    // }
    // else if($count == 4)
    // {
    //      $slide_value = $count.'00';
    //      $item_value = '';
    // }

	?>
    

    <!-- SLIDE  -->
    <li data-index="rs-<?php echo $slide_value?>" data-transition="parallaxvertical" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="<?php echo $slideshow_location.$slideshow_image_name;?>"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="ROYAL SMILES" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="">
        <!-- MAIN IMAGE -->
        <img src="<?php echo $slideshow_location.$slideshow_image_name;?>"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
        <!-- LAYERS -->
        <div class="tp-caption tp-shape tp-shapewrapper " id="slide-<?php echo $slide_value?>-layer-1<?php echo $item_value?>" 
        data-x="['center','center','center','center']" 
        data-hoffset="['0','0','0','0']" 
        data-y="['middle','middle','middle','middle']" 
        data-voffset="['0','0','0','0']" 
        data-width="full" data-height="full" 
        data-whitespace="nowrap" 
        data-type="shape" 
        data-basealign="slide" 
        data-responsive_offset="off" 
        data-responsive="off" 
        data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power4.easeOut"}]' 
        data-textAlign="['left','left','left','left']" 
        data-paddingtop="[0,0,0,0]" 
        data-paddingright="[0,0,0,0]" 
        data-paddingbottom="[0,0,0,0]" 
        data-paddingleft="[0,0,0,0]" 
        style="z-index: 2;background-color:rgba(102, 45, 145, 0.3);border-color:rgba(102, 45, 145,, 0);border-width:0px; "> </div>
        <!-- LAYER NR. 2 -->
        <div class="tp-caption Newspaper-Title tp-resizeme" 
            id="slide-<?php echo $slide_value?>-layer-3<?php echo $item_value?>" 
            data-x="['left','left','left','left']" 
            data-hoffset="['50','50','50','30']" 
            data-y="['top','top','top','top']" 
            data-voffset="['220','220','240','100']" 
            data-fontsize="['45','45','40','28']"
            data-lineheight="['85','85','50','35']"
            data-width="['1000','1000','1000','420']"
            data-height="none"
            data-whitespace="normal"
 
            data-type="text" 
            data-responsive_offset="on" 

            data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[10,10,10,10]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 6; white-space: normal;text-transform:uppercase; font-weight:700; line-height:50px; font-family: 'rubik', sans-serif; color:#fff;"><span class="text-primary"><?php echo $first_name?></span> <?php echo $second_name;?></div>
        <!-- LAYER NR. 3 -->
        <div class="tp-caption Newspaper-Title   tp-resizeme" 
            id="slide-<?php echo $slide_value?>-layer-4<?php echo $item_value?>" 
            data-x="['left','left','left','left']" 
            data-hoffset="['50','50','50','30']" 
            data-y="['top','top','top','top']" 
            data-voffset="['310','310','310','145']" 
            data-fontsize="['16','15','14','13']"
            data-lineheight="['26','25','24','23']"
            data-width="['700','600','600','420']"
            data-height="none"
            data-whitespace="normal"
 
            data-type="text" 
            data-responsive_offset="on" 

            data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[10,10,10,10]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 6; white-space: normal;text-transform:left; line-height:80px; color:#fff; font-family:'rubik', sans-serif"><?php echo $slideshow_description?></div>
        <!-- LAYER NR. 4 -->
        <div class="tp-caption" 
            id="slide-<?php echo $slide_value?>-layer-5<?php echo $item_value?>" 
            data-x="['left','left','left','left']" data-hoffset="['53','53','50','30']" 
            data-y="['top','top','top','top']" data-voffset="['410','410','410','250']" 
            data-width="none"
            data-height="none"
            data-whitespace="nowrap"
            data-responsive_offset="on" 
            data-responsive="off"
            data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);bw:1px 1px 1px 1px;"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 8; white-space: nowrap; background-color:rgba(102, 45, 145,, 0) !important;"> <a href="<?php echo site_url().'about-us'?>" class="site-button button-lg">Read More </a> </div>
        <div class="tp-caption" 
            id="slide-<?php echo $slide_value?>-layer-6<?php echo $item_value?>" 
            data-x="['left','left','left','left']" data-hoffset="['230','230','210','180']" 
            data-y="['top','top','top','top']" data-voffset="['410','410','410','250']" 
            data-width="none"
            data-height="none"
            data-whitespace="nowrap"
            data-responsive_offset="on" 
            data-responsive="off"
            data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);bw:1px 1px 1px 1px;"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 8; white-space: nowrap; background-color:rgba(102, 45, 145,, 0) !important;"> <a href="<?php echo site_url().'contact-us'?>" class="site-button  button-lg outline white">Contact Us</a> </div>
    </li>    
   
        
<?php

         $z+100;
         $y=1;
	}
}
?>
		