
        <!-- PANEL 5 -->
        <div class="container pm-containerPadding-top-120 pm-containerPadding-bottom-80">
        	
            <div class="row">
            	<div class="col-lg-12 pm-center pm-column-spacing">
                	<h5>Our Services</h5>
                    <div class="pm-column-title-divider">
               	    	<img src="<?php echo base_url().'assets/themes/medicallink/'?>img/net.jpg" width="29" height="29" alt="icon">
                    </div>
                </div>
            </div>
        
        	<div class="row">
            	<div class="col-lg-4 col-md-4 col-sm-12 pm-column-spacing">
                	
                    <!-- Staff profile -->
                    <div class="pm-staff-profile-parent-container wow fadeInUp animated" data-wow-delay="0.3s" data-wow-offset="50" data-wow-duration="1s">
                    
                    	<div class="pm-staff-profile-container" style="background-image:url(<?php echo base_url().'assets/images/6.png'?>);">
                    
                            <div class="pm-staff-profile-overlay-container">
                                
                                <div class="pm-staff-profile-quote">
                                    <p>"We provide end to end event planning services. We handle all aspects of the event from budgeting, theme creation, supplier selection, venue search, guest management, program creation and ticket sales."</p>
                                </div>
                            
                            </div>
                                                    
                            <a href= data-rel="prettyPhoto" class="pm-staff-profile-expander fa fa-plus"></a>
                                                
                        </div>
                        
                        <div class="pm-staff-profile-info">
                            <p class="pm-staff-profile-title"><h2><i>COOPERATE EVENTS</i></h2></p>
                            <p class="pm-staff-profile-title"><i>Rose 'n Roses<i/></p>
                            <p class="pm-staff-profile-title">We provide end to end event planning services. We handle all aspects of the event from budgeting, theme creation, supplier selection, venue search, guest management, program creation and ticket sales.</p>
                        </div>
                    
                    </div>
                    
                    
                    <!-- Staff profile end -->
                    
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 pm-column-spacing">
                	
                    <!-- Staff profile -->
                    <div class="pm-staff-profile-parent-container wow fadeInUp animated" data-wow-delay="0.6s" data-wow-offset="50" data-wow-duration="1s">
                    
                    	<div class="pm-staff-profile-container" style="background-image:url(<?php echo base_url().'assets/images/gh.jpg'?>);">
                    
                            <div class="pm-staff-profile-overlay-container">
                                
                                <div class="pm-staff-profile-quote">
                                    <p>The wedding planning process includes all aspects of planning including but not limited venue research and catering, supplier research, costing and confirmations, styling and conceptualization, flowers, décor and draping services, stationery design and DJ and Entertainment Services. On the day coordination including venue management, supplier coordination, overseeing the setup and breakdown of all services, assistance to the bridal party and running the agenda for the day</p>
                                </div>
                            
                            </div>
                                                    
                            <a href= data-rel="prettyPhoto" class="pm-staff-profile-expander fa fa-plus"></a>
                                                
                        </div>
                        
                        
                        <div class="pm-staff-profile-info">
                            <p class="pm-staff-profile-title"><h2>WEDDINGS EVENTS</h2></p>
                            <p class="pm-staff-profile-title"><i>Rose 'n Roses<i/></p>
                            <p class="pm-staff-profile-title">The wedding planning process includes all aspects of planning including but not limited venue research and catering, supplier research, costing and confirmations, styling and conceptualization, flowers, décor and draping services, stationery design and DJ and Entertainment Services. On the day coordination including venue management, supplier coordination, overseeing the setup and breakdown of all services, assistance to the bridal party and running the agenda for the day</p>
                        </div>
                    
                    </div>                    
                    <!-- Staff profile end -->
                    
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                	
                    <!-- Staff profile -->
                    <div class="pm-staff-profile-parent-container wow fadeInUp animated" data-wow-delay="0.9s" data-wow-offset="50" data-wow-duration="1s">
                    
                    	<div class="pm-staff-profile-container" style="background-image:url(<?php echo base_url().'assets/images/food.jpg'?>);">
                    
                            <div class="pm-staff-profile-overlay-container">
                                
                                <div class="pm-staff-profile-quote">
                                    <p>We also provide beverages, food, and other services to different clients for all our events. Caterers are some of the professionals working on this area and their duties are related to organize and control catering operations.</p>
                                </div>
                            
                            </div>
                                                    
                            <a href= data-rel="prettyPhoto" class="pm-staff-profile-expander fa fa-plus"></a>
                                                
                        </div>
                        
                        <div class="pm-staff-profile-info">
                            <p class="pm-staff-profile-title"><h2>CATERING SERVICES</h2></p>
                            <p class="pm-staff-profile-title">Rose 'n' Roses</p>
                             <p class="pm-staff-profile-title">We also provide beverages, food, and other services to different clients for all our events. Caterers are some of the professionals working on this area and their duties are related to organize and control catering operations</p>
                        </div>
                        
                    </div>                    
                    <!-- Staff profile end -->
                    
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
        <!-- PANEL 5 end -->
        