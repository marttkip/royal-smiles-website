<?php

$contacts = $this->site_model->get_contacts();

  if(count($contacts) > 0)
  {
    $email = $contacts['email'];
    $phone = $contacts['phone'];
    $facebook = $contacts['facebook'];
    $twitter = $contacts['twitter'];
    $linkedin = $contacts['linkedin'];
    $instagram = $contacts['instagram'];
    $logo = $contacts['logo'];
    $company_name = $contacts['company_name'];
    $about = $contacts['about'];
    $address = $contacts['address'];
    $city = $contacts['city'];
    $post_code = $contacts['post_code'];
    $building = $contacts['building'];
    $floor = $contacts['floor'];
    $location = $contacts['location'];
    $working_weekend = $contacts['working_weekend'];
    $working_weekday = $contacts['working_weekday'];
    
    if(!empty($email))
    {
      $mail = '<div class="top-number"><p><i class="fa fa-envelope-o"></i> '.$email.'</p></div>';
    }
    
    // if(!empty($facebook))
    // {
    //  $facebook = '<li><a class="social_icon facebook" href="'.$facebook.'" target="_blank"></a></li>';
    // }
    
    // if(!empty($twitter))
    // {
    //  $twitter = '<li><a class="social_icon twitter" href="'.$twitter.'" target="_blank"></li>';
    // }
    
    // if(!empty($linkedin))
    // {
    //  $linkedin = '<li><a class="social_icon googleplus" href="'.$linkedin.'" target="_blank"></li>';
    // }
  }
  else
  {
    $email = '';
    $facebook = '';
    $twitter = '';
    $linkedin = '';
    $logo = '';
    $instagram = '';
    $company_name = '';
  }
$popular_query = $this->blog_model->get_popular_posts();

if($popular_query->num_rows() > 0)
{
    $popular_posts = '';
    $count = 0;
    foreach ($popular_query->result() as $row)
    {
        $count++;
        
        if($count < 3)
        {
            $post_id = $row->post_id;
            $post_title = $row->post_title;
            $image = base_url().'assets/images/posts/thumbnail_'.$row->post_image;
            $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
            $description = $row->post_content;
            $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 10));
            $created = date('jS M Y',strtotime($row->created));
            
            $popular_posts .= '

                          <li class="icon_small_arrow right_white">
                            <a href="'.site_url().'blog/view-single/'.$post_id.'">
                             '.$mini_desc.'.
                            </a>
                            <abbr title="'.$created.'" class="timeago">'.$created.'</abbr>
                          </li>
                
            ';
        }
    }
}

else
{
    $popular_posts = 'There are no posts yet';
}


  $branches_rs = $this->site_model->get_all_branches();

    $branches_list = '';
    if($branches_rs->num_rows() > 0)
    {
        foreach ($branches_rs->result() as $key => $value) {
            # code...
            $branch_name = $value->branch_name;
            $branch_email = $value->branch_email;
            $branch_phone = $value->branch_phone;
            $branch_address = $value->branch_address;
            // $branch_postal_code = $value->branch_postal_code;
            $branch_location = $value->branch_location;
            $branch_building = $value->branch_building;
            $branch_floor = $value->branch_floor;
            $location_link = $value->location_link;
            $web_name = $this->site_model->create_web_name($branch_name);
            
          $branches_list .= '
                            <div class="col-md-4">
                                <ul>
                                    <li>
                                    <b><a href="'.site_url().'view-branch/'.$web_name.'">'.$branch_name.'</a></b>
                                     <p> '.$branch_phone.'</p>
                                    </li>
                                </ul>
                            </div>';
    }
  }



?>

 <footer class="site-footer footer-overlay bg-img-fix" style="background-image: url(<?php echo base_url().'assets/themes/theme/'?>images/background/maxi.jpg); background-position: center bottom; background-size: cover;">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6 footer-col-4">
                        <div class="widget widget_about">
                            <div class="logo-footer"><img src="<?php echo base_url().'assets/logo/'.$logo?>" alt=""></div>
                            <p class="m-tb20"> <?php echo $about?></p>
                            <ul class="dez-social-icon border dez-social-icon-lg">
                            <li><a href="<?php echo $facebook?>" target="_BLANK" class="fa fa-facebook"></a></li>
                            <li><a href="<?php echo $twitter?>" target="_BLANK" class="fa fa-twitter"></a></li>
                            <li><a href="<?php echo $linkedin?>" target="_BLANK" class="fa fa-linkedin "></a></li>
                            <li><a href="<?php echo $instagram?>" target="_BLANK" class="fa fa-instagram"></a></li>
                            </ul> <br> <br>
                            <ul class="dez-social-icon border dez-social-icon-lg">
                            <li><img src="<?php echo base_url().'assets/themes/theme/'?>images/client-logo/RSDC-Associates(1).png" alt="" style="width: 150px"></li>
                            </ul>
                        </div>
                        
                    </div>
                   
                    <div class="col-lg-8 col-md-8 col-sm-8 footer-col-8">


                        <div class="widget widget_getintuch">
                            <h4 class="m-b15 text-uppercase">Talk To Us</h4>
                            <div class="dez-separator bg-primary"></div>
                            <div class="row">
                               <?php echo $branches_list;?>
                            </div>
                             <!-- <div class="row" style="margin-top: 40px">
                                <div class="col-md-12 " style="margin:0 auto !important; width: auto !important;">
                                    <a href="<?php echo site_url().'services'?>">Services</a> | 
                                    <a href="<?php echo site_url().'our-branches'?>">Branches</a> |
                                    <a href="<?php echo site_url().'whats-new'?>">What's New</a> |
                                    <a href="<?php echo site_url().'book-appointment'?>">Book Appointment</a> |
                                    <a href="<?php echo site_url().'faqs'?>">FAQs</a> 
                                </div>
                            </div> -->
                                      
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
        <!-- footer bottom part -->
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 text-left">
                        <span>Copyright © <?php echo date("Y");?> Royal Smiles Dental Clinic</span>
                    </div>
                    <div class="col-lg-4 col-md-4 text-center">
                        <span> Designed With <i class="fa fa-heart heart"></i> By<a target="_BLANK" href="minesoftwares.com"> Mine Softwares </a></span> 
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <ul class="footer-info-list text-right">
                            <li><a href="<?php echo site_url().'our-branches'?>">  Branches</a></li>
                            <li><a href="<?php echo site_url().'our-team'?>">  Our Doctors</a></li>
                            <li><a href="<?php echo site_url().'whats-new'?>"> What's New </a></li>
                            <!-- <li><a href="<?php echo site_url().'book-appointment'?>"> Book Appointment </a></li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
