    <?php
    // $mini_desc = '';
    if(!isset($meta_tags) OR empty($meta_tags))
    {
        $contacts = $this->site_model->get_contacts();
        $post_meta = strip_tags($contacts['post_meta']);
        $mini_desc =strip_tags($contacts['vision']);
        
    }
    else
    {
        $post_meta = strip_tags($meta_tags);
        $post_meta = strip_tags($meta_tags);
    }
    ?>

    <head>
        <title><?php echo $title;?></title>
        <!--meta-->
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <meta name="author" content="Admin" />
        <meta name="MobileOptimized" content="320" />



        <meta name="description" content="<?php echo $mini_desc;?>" />
        <meta name="keywords" content="<?php echo $post_meta;?>" />
        <meta property="og:title" content="<?php echo $title;?>" />
        <meta property="og:description" content="<?php echo $mini_desc;?>" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON -->

        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/";?>fontawesome/css/font-awesome.min.css"
            type="text/css">
        <link rel="icon" href="<?php echo base_url().'assets/themes/theme/'?>images/favicon.png" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon"
            href="<?php echo base_url().'assets/themes/theme/'?>images/favicon.png" />

        <!-- PAGE TITLE HERE -->

        <!-- MOBILE SPECIFIC -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->

        <!-- STYLESHEETS -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/themes/theme/'?>css/plugins.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/themes/theme/'?>css/style.min.css">
        <link class="skin" rel="stylesheet" type="text/css"
            href="<?php echo base_url().'assets/themes/theme/'?>css/skin/skin-1.css">
        <link rel="stylesheet" type="text/css"
            href="<?php echo base_url().'assets/themes/theme/'?>css/templete.min.css">
        <!-- Revolution Slider Css -->
        <link rel="stylesheet" type="text/css"
            href="<?php echo base_url().'assets/themes/theme/'?>plugins/revolution/revolution/css/layers.css">
        <link rel="stylesheet" type="text/css"
            href="<?php echo base_url().'assets/themes/theme/'?>plugins/revolution/revolution/css/settings.css">
        <link rel="stylesheet" type="text/css"
            href="<?php echo base_url().'assets/themes/theme/'?>plugins/revolution/revolution/css/navigation.css">
        <!-- Revolution Navigation Style -->
        <!-- Font Awesome -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" />
    </head>