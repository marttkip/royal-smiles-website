<?php
	$company_details = $this->site_model->get_contacts();
	
	if(count($company_details) > 0)
	{
		$email = $company_details['email'];
		$email2 = $company_details['email'];
		$facebook = $company_details['facebook'];
		$twitter = $company_details['twitter'];
		$linkedin = '';// $company_details['linkedin'];
		$logo = $company_details['logo'];
		$company_name = $company_details['company_name'];
		$phone = $company_details['phone'];
        $google = '';
		
		// if(!empty($email))
		// {
		// 	$email = '<div class="top-number"><p><i class="fa fa-envelope-o"></i> '.$email.'</p></div>';
		// }
		
		// if(!empty($facebook))
		// {
		// 	$twitter = '<li class="pm_tip_static_bottom" title="Twitter"><a href="#" class="fa fa-twitter" style="border: 3px solid #B22222; color: #B22222; " target="_blank"></a></li>';
		// }
		
		// if(!empty($facebook))
		// {
		// 	$linkedin = '<li class="pm_tip_static_bottom" title="Linkedin"><a href="#" class="fa fa-linkedin" style="border: 3px solid #B22222; color: #B22222; " target="_blank"></a></li>';
		// }
		// if(!empty($facebook))
		// {
		// 	$instagram = '<li class="pm_tip_static_bottom" title="Instagram"><a href="#" class="fa fa-instagram" style="border: 3px solid #B22222; color: #B22222; " target="_blank"></a></li>';
		// }
		
		// if(!empty($facebook))
		// {
		// 	$google = '<li class="pm_tip_static_bottom" title="Google Plus"><a href="#" class="fa fa-google-plus" style="border: 3px solid #B22222; color: #B22222; " target="_blank"></a></li>';
		// }
		
		// if(!empty($facebook))
		// {
		// 	$facebook = '<li class="pm_tip_static_bottom" title="Facebook"><a href="#" class="fa fa-facebook" style="border: 3px solid #B22222; color: #B22222; " target="_blank"></a></li>';
		// }
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
		$logo = '';
		$company_name = '';
		$google = '';
	}
    // var_dump($logo);die()


?>


<header class="site-header header header-style-2 dark mo-left">
        <!-- top bar -->
        <div class="top-bar">
            <div class="container">
                <div class="row d-flex justify-content-between">
                    <div class="dez-topbar-left">
                        <ul class="social-line text-center pull-right">
                            
                            <li><a href="javascript:void(0);"><i class="fa fa-home"></i> <span>Head Clinic</span></a></li>
                            <li><a href="javascript:void(0);"><i class="fa fa-phone"></i> <span><?php echo $phone?></span> </a></li>
                            <li><a href="javascript:void(0);"><i class="fa fa-envelope-o"></i> <span><?php echo $email?></span></a></li>
                        </ul>
                    </div>
                    <div class="dez-topbar-right">
                        <ul class="social-line text-center pull-right">
                            <li><a href="<?php echo $facebook?>" target="_BLANK" class="fa fa-facebook"></a></li>
                            <li><a href="<?php echo $twitter?>" target="_BLANK" class="fa fa-twitter"></a></li>
                            <li><a href="<?php echo $linkedin?>" target="_BLANK" class="fa fa-linkedin"></a></li>
                            <li><a href="<?php echo $google?>" target="_BLANK" class="fa fa-instagram"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- top bar END-->
        <!-- main header -->
        <div class="sticky-header  main-bar-wraper navbar-expand-lg">
            <div class="main-bar clearfix ">
                <div class="container clearfix">
                    <!-- website logo -->
                    <div class="logo-header mostion"><a href="<?php echo site_url().'home'?>"><img src="<?php echo base_url().'assets/logo/'.$logo?>" width="253" height="100" alt=""></a></div>
                    <!-- nav toggle button -->
                    <button class="navbar-toggler collapsed navicon justify-content-end" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <!-- extra nav -->
                    <div class="extra-nav">
                        <div class="extra-cell">
                            <a href="<?php echo site_url().'book-appointment'?>" id="quik-search-btn" type="button" class="site-button"><i class="fa fa-calendar"></i>  Book Appointment</a>
                        </div>
                    </div>
                    <!-- Quik search -->
                    <!-- <div class="dez-quik-search bg-primary ">
                        <form action="#">
                            <input name="search" value="" type="text" class="form-control" placeholder="Type to search">
                            <span  id="quik-search-remove"><i class="fa fa-remove"></i></span>
                        </form>
                    </div> -->
                    <!-- main nav -->
                    <div class="header-nav navbar-collapse collapse justify-content-end" id="navbarNavDropdown">
                        <ul class="nav navbar-nav">
                            <?php echo $this->site_model->get_navigation();?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- main header END -->
    </header>
