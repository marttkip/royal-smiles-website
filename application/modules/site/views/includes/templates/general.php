<!DOCTYPE html>
<html lang="en">
	<?php echo $this->load->view('includes/header', '', TRUE);?>
	
    <body id="bg">
    	<div class="page-wraper">
	        <?php echo $this->load->view('includes/navigation', '', TRUE);?>

    		<script src='https://www.google.com/recaptcha/api.js'></script>

	        <?php echo $content; ?>
	        <?php echo $this->load->view('includes/footer', '', TRUE);?>

	        <button class="scroltop fa fa-chevron-up" ></button>
      	</div>
         <!--footer wrapper end-->
	    <!--main js files-->

	    <!-- JavaScript  files ========================================= -->
		<script src="<?php echo base_url().'assets/themes/theme/'?>js/jquery.min.js"></script><!-- JQUERY.MIN JS -->
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/bootstrap/js/popper.min.js"></script><!-- BOOTSTRAP.MIN JS -->
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/bootstrap/js/bootstrap.min.js"></script><!-- BOOTSTRAP.MIN JS -->
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/bootstrap-select/bootstrap-select.min.js"></script><!-- FORM JS -->
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script><!-- FORM JS -->
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/magnific-popup/magnific-popup.js"></script><!-- MAGNIFIC POPUP JS -->
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/counter/waypoints-min.js"></script><!-- WAYPOINTS JS -->
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/counter/counterup.min.js"></script><!-- COUNTERUP JS -->
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/imagesloaded/imagesloaded.js"></script><!-- IMAGESLOADED -->
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/masonry/masonry-3.1.4.js"></script><!-- MASONRY -->
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/masonry/masonry.filter.js"></script><!-- MASONRY -->
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/owl-carousel/owl.carousel.js"></script><!-- OWL SLIDER -->
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/lightgallery/js/lightgallery-all.js"></script><!-- LIGHT GALLERY -->
		<script src="<?php echo base_url().'assets/themes/theme/'?>js/custom.min.js"></script><!-- CUSTOM FUCTIONS  -->
		<script src="<?php echo base_url().'assets/themes/theme/'?>js/dz.carousel.min.js"></script><!-- SORTCODE FUCTIONS  -->
		<script src="<?php echo base_url().'assets/themes/theme/'?>js/dz.ajax.js"></script><!-- CONTACT JS  -->
		<!-- revolution JS FILES -->
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/revolution/revolution/js/jquery.themepunch.tools.min.js"></script>
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/revolution/revolution/js/jquery.themepunch.revolution.min.js"></script>
		<!-- Slider revolution 5.0 Extensions  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/revolution/revolution/js/extensions/revolution.extension.actions.min.js"></script>
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/revolution/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/revolution/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/revolution/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/revolution/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/revolution/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/revolution/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
		<script src="<?php echo base_url().'assets/themes/theme/'?>plugins/revolution/revolution/js/extensions/revolution.extension.video.min.js"></script>
		<script src="<?php echo base_url().'assets/themes/theme/'?>js/rev.slider.js"></script>
		<script>
		jQuery(document).ready(function() {
			'use strict';
			dz_rev_slider_3();
		});	/*ready*/
		</script>
		<script type="text/javascript">
			// Query(document).ready(function($){
		

		</script>
	    

      
    
	</body>
</html>