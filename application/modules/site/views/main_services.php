<?php

         $blog_category_id = $this->site_model->get_category_id('Company Services');

        $category_items = $this->site_model->get_active_post_content_by_category($blog_category_id);
        
        $main_services = '';
        if($category_items->num_rows() > 0)
        {
          
         
          foreach ($category_items->result() as $key => $row) {
            # code...

            $post_title = $row->post_title;

             $post_id = $row->post_id;
            $blog_category_name = $row->blog_category_name;
            $blog_category_id = $row->blog_category_id;
            $post_title = $row->post_title;
            $web_name = $this->site_model->create_web_name($post_title);
            $post_status = $row->post_status;
            $post_views = $row->post_views;
            $image_specialist = base_url().'assets/images/posts/'.$row->post_image;
            $created_by = $row->created_by;
            $modified_by = $row->modified_by;
            $post_target = $row->post_target;
            $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
            $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
            $description = $row->post_content;
            $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
            $created = $row->created;
            $day = date('j',strtotime($created));
            $month = date('M',strtotime($created));
            $year = date('Y',strtotime($created));
            $created_on = date('jS M Y',strtotime($row->created));
              
            $web_name = $this->site_model->create_web_name($post_title);

            $main_services .= '
                              <div class="col-lg-3 col-md-6 col-sm-6 m-b30">
                                <div class="dez-box">
                                    <div class="dez-media"> <a href="#"><img src="'.$image_specialist.'" alt=""></a> </div>
                                    <div class="dez-info p-a30 border-1">
                                        <h4 class="dez-title m-t0"><a href="'.site_url().'view-service-category/'.$web_name.'">'.$post_title.'</a></h4>
                                        <div class="dez-separator bg-primary"></div>
                                        <p class="m-b15"></p>
                                        <a href="'.site_url().'view-service-category/'.$web_name.'" class="site-button btn-block">Read More</a> 
                                    </div>
                                </div>
                            </div>
                          ';

          }
         
         
        }
        else
        {
          // put that category here
        }

  

  ?>


<?php echo $main_services?>

