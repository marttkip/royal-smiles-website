<?php 

$branches_rs = $this->site_model->get_all_branches();

$branches_list = '';
if($branches_rs->num_rows() > 0)
{
    foreach ($branches_rs->result() as $key => $value) {
        # code...
        $branch_name = $value->branch_name;
        $branch_email = $value->branch_email;
        $branch_phone = $value->branch_phone;
        $branch_address = $value->branch_address;
        // $branch_postal_code = $value->branch_postal_code;
        $branch_location = $value->branch_location;
        $branch_building = $value->branch_building;
        $branch_floor = $value->branch_floor;
        $location_link = $value->location_link;
         $web_name = $this->site_model->create_web_name($branch_name);

        $branches_list .=' <div class="col-md-4 d-flex">
                            <div class="p-a30 m-b30 border contact-area border-1 align-self-stretch">
                                <h2 class="m-b10">'.$branch_name.'</h2>
                                <ul class="no-margin">
                                    <li class="icon-bx-wraper left m-b30">
                                        <div class="icon-bx-xs border-1">
                                            <a href="#" class="icon-cell"><i class="fa fa-map-marker"></i></a>
                                        </div>
                                        <div class="icon-content">
                                            <h6 class="text-uppercase m-tb0 dez-tilte">Address:</h6>
                                            <p> '.$branch_building.' '.$branch_floor.' '.$branch_location.'</p>
                                        </div>
                                    </li>
                                    <li class="icon-bx-wraper left m-b30">
                                        <div class="icon-bx-xs border-1">
                                            <a href="#" class="icon-cell"><i class="fa fa-envelope"></i></a>
                                        </div>
                                        <div class="icon-content">
                                            <h6 class="text-uppercase m-tb0 dez-tilte">Email:</h6>
                                            <p>'.$branch_email.'</p>
                                        </div>
                                    </li>
                                    <li class="icon-bx-wraper left">
                                        <div class="icon-bx-xs border-1">
                                            <a href="#" class="icon-cell"><i class="fa fa-phone"></i></a>
                                        </div>
                                        <div class="icon-content">
                                            <h6 class="text-uppercase m-tb0 dez-tilte">PHONE</h6>
                                            <p>'.$branch_phone.'</p>
                                        </div>
                                    </li>
                                </ul>
                                <div class="m-t20">
                                    <a
                                        href="'.site_url().'view-branch/'.$web_name.'"
                                       
                                        class="site-button text-center"
                                    >
                                        <span>View Details</span>
                                    </a>
                                </div>
                            </div>
                        </div>';
    }
}
?>
<div class="page-content">
    <!-- inner page banner -->
    <div class="dez-bnr-inr overlay-black-middle" style="background-image: url(<?php echo base_url().'assets/themes/theme/'?>images/banner/bnr1.jpg);">
        <div class="container">
            <div class="dez-bnr-inr-entry">
                <h1 class="text-white"><?php echo $title?></h1>
            </div>
        </div>
    </div>
    <!-- inner page banner END -->

    <!-- inner page banner -->
    <!--         <div>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d227748.3825624477!2d75.65046970649679!3d26.88544791796718!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x396c4adf4c57e281%3A0xce1c63a0cf22e09!2sJaipur%2C+Rajasthan!5e0!3m2!1sen!2sin!4v1500819483219" style="border:0; width:100%; margin-bottom: -6px; height:300px;" allowfullscreen></iframe>
		</div> -->
    <!-- inner page banner END -->
    <!-- Breadcrumb row -->
    <div class="breadcrumb-row">
        <div class="container">
            <ul class="list-inline">
                <li><a href="<?php echo site_url().'home'?>">Home</a></li>
                <li>Our branches</li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumb row END -->
    <!-- contact area -->
    <div class="section-full content-inner bg-white contact-style-1">
        <div class="container">
            <div class="row">
                    <?php echo $branches_list;?>
                
            </div>
        </div>
    </div>
    <!-- contact area  END -->
</div>

<?php echo $this->load->view("site/our_partners", '');?>  
