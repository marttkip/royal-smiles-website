<?php
  $doctors_list_query = $this->site_model->get_active_items('Our Doctors');
   $doctors_list = '';
  if($doctors_list_query->num_rows() > 0)
  {
    $x=0;
    foreach($doctors_list_query->result() as $row)
    {
      $about_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $doctors_list_image_about = base_url().'assets/images/posts/'.$row->post_image;
      $created_by = $row->created_by;
      $modified_by = $row->modified_by;
      $post_target = $row->post_target;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));
      $x++;

      $doctors_list .= '
      					<div class="col-lg-4 col-md-4 m-b30">
                            <div class="dez-box">
                                <div class="dez-media"> <a href="#"><img src="'.$doctors_list_image_about.'" alt=""></a> </div>
                                <div class="dez-info p-a20 p-t40 border-1">
                                    <h4 class="dez-title m-tb0"><a href="#">'.$post_target.'</a></h4>
                                    <div class="bg-primary skew-content-box">
                                    '.$post_title.'
                                        
                                    </div>
                                    <p class="m-t10 m-b0">'.$description.'</p>
                                </div>
                            </div>
                        </div>';
    }
  }
  ?>
<div class="page-content">
    <!-- inner page banner -->
    <div class="dez-bnr-inr overlay-black-middle" style="background-image: url(<?php echo base_url().'assets/themes/theme/'?>images/banner/bnr1.jpg);">
        <div class="container">
            <div class="dez-bnr-inr-entry">
                <h1 class="text-white"><?php echo $title?></h1>
            </div>
        </div>
    </div>
    
    <div class="breadcrumb-row">
        <div class="container">
            <ul class="list-inline">
                <li><a href="<?php echo site_url().'home'?>">Home</a></li>
                <li>Our Doctors</li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumb row END -->
    <!-- contact area -->
    <div class="section-full content-inner bg-white contact-style-1">
        <div class="container">
            <div class="row">
                    <div class="section-full bg-white content-inner">
		                <div class="container">
		                    <div class="section-head text-center ">
		                        <h3 class="h3">Meet our Dedicate Team</h3>
		                        <div class="dez-separator bg-primary"></div>
		                        <p>Royals smiles Dental clinic have qualified and dedicated team that will always be there for your dental issues.</p>
		                    </div>
		                    <div class="section-content ">
		                        <div class="row">
		                        	<?php echo $doctors_list?>
		                           
		                            
		                        </div>
		                    </div>
		                </div>
		            </div> 
                
            </div>
        </div>
    </div>
    <!-- contact area  END -->
</div>