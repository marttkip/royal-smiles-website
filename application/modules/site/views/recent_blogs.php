<?php
$about_query = $this->site_model->get_active_items('Company Blog',10);
 $blog_list = '';

 // var_dump($about_query);die();
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $about_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $post_title = $row->post_title;
    $web_name = $this->site_model->create_web_name($post_title);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $personnel_fname = $row->personnel_fname;
    $image_about = base_url().'assets/images/posts/'.$row->post_image;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $post_target = $row->post_target;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $description = $row->post_content;
    $mini_desc = strip_tags(implode(' ', array_slice(explode(' ', $description), 0, 50)));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));
    $personnel_fname = $row->personnel_fname;

    if(empty($personnel_fname))
    {
        $personnel_fname = 'Admin';
    }
    $x++;
    if($x < 9)
    {
      $x = '0'.$x;
    }
    $blog_list .= '
    					

                                <div class="item">
                                    <div class="dez-box">
                                        <div class="dez-media"> 
                                            <a href="#"><img src="'.$image_about.'" alt="" style="max-height:255px;"></a> 
                                        </div>
                                        <div class="dez-info p-t20">
                                            <div class="dez-post-meta">
                                                <ul>
                                                    <li class="post-date"> <i class="fa fa-calendar"></i><strong>'.$day.' '.$month.'</strong> <span> '.$year.'</span> </li>
                                                        <li class="post-author"><i class="fa fa-user"></i>By <a href="#">'.$personnel_fname.'</a> </li>
                                                </ul>
                                            </div>
                                            <h4 class="dez-title m-t15"><a href="#">'.$post_title.'</a></h4>
                                                <div class="dez-post-text">
                                                    '.$mini_desc.'
                                                </div>
                                            <a href="#" class="site-button ">Read More</a> 
                                        </div>
                                    </div>
                                </div>
			                ';
  }
}
?>
   <!-- About Company END -->
        <!-- Latest blog -->
        <div class="section-full content-inner-1 bg-gray">
            <div class="container">
                <div class="section-head text-center">
                    <h3 class="h3 text-uppercase">What's  <span class="text-primary">New!</span></h3>
                </div>
                <div class="section-content ">
                    <div class="owl-carousel testimonial-one blog-carousel owl-btn-1 primary owl-btn-center-lr">

                        <?php echo $blog_list;?>
                    </div>
                </div>
    
            </div>
        </div>