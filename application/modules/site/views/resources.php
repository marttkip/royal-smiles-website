<div class="page-content">
    <!-- inner page banner -->
    <div class="dez-bnr-inr overlay-black-middle"
        style="background-image:url(<?php echo base_url().'assets/themes/theme/'?>images/banner/bnr1.jpg);">
        <div class="container">
            <div class="dez-bnr-inr-entry">
                <h1 class="text-white">Resources</h1>
            </div>
        </div>
    </div>
    <!-- inner page banner END -->
    <!-- Breadcrumb row -->
    <div class="breadcrumb-row">
        <div class="container">
            <ul class="list-inline">
                <li><a href="<?php echo site_url().'home';?>">Home</a></li>
                <li>Resources</li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumb row END -->
    <!-- contact area -->
    <div class="clearfix">
        <!-- Our Awesome Services -->
        <div class="section-full bg-white content-inner">
            <div class="container">
                <div class="section-content">
                    <div class="text-center section-head">
                        <h3 class="h3">Royal Smiles Dental Clinic Resources</h3>
                        <div class="dez-separator bg-primary"></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <?php 
                if ($query->num_rows() > 0)
                { ?>
            <div class="container">
                <div class="card-columns">
                    <?php 
                    foreach ($query->result() as $row){
                    $resource_id = $row->resource_id;
                    $resource_name = $row->resource_name;
                    $resource_title = $row->resource_title; ?>
                    <div class="card border-dark rounded-top">
                        <div class="card-body text-center">
                            <i class="far fa-file-pdf fa-8x text-danger"></i>
                            <h5 class=" pt-2 card-title"><?php echo $resource_title; ?></h5>
                            <div class="row">
                                <div class="col-6">
                                    <a href="<?php echo base_url().'assets/resources/'.$resource_name; ?>"
                                        target="_BLANK"
                                        class="btn btn-sm btn-outline-dark btn-block font-weight-bold">READ <i
                                            class="fas fa-eye"></i></a>
                                </div>
                                <div class="col-6">
                                    <a href="<?php echo base_url().'assets/resources/'.$resource_name; ?>"
                                        target="_BLANK" download
                                        class="btn btn-sm btn-outline-dark btn-block font-weight-bold">DOWNLOAD <i
                                            class="fas fa-cloud-download-alt"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <?php } else {?>
                <div class="container py-4">
                    <div class="p-5 mb-4 bg-light rounded-3">
                        <div class="container-fluid py-5">
                            <h1 class="display-5 fw-bold">No resources to display at the moment</h1>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
            <!-- Our Awesome Services END -->
            <!-- Company Stats  -->

            <!-- Company Stats END -->
            <!-- Testimonials End -->
        </div>
    </div>