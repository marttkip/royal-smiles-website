<?php
  $about_query = $this->site_model->get_active_content_items($title,1);
   $about_company = '';
  if($about_query->num_rows() == 1)
  {
    $x=0;
    foreach($about_query->result() as $row)
    {
      $about_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      // $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $image_about = base_url().'assets/images/posts/'.$row->post_image;
      $created_by = $row->created_by;
      $modified_by = $row->modified_by;
      $post_target = $row->post_target;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));
      $x++;
      if($x < 9)
      {
        $x = '0'.$x;
      }

      $gallery_rs = $this->site_model->get_post_gallery($post_id);

      $gallery = '';
      if($gallery_rs->num_rows() > 0)
      {
        foreach ($gallery_rs->result() as $key => $value) {
          # code...
          $post_gallery_image_name = base_url().'assets/images/posts/'.$value->post_gallery_image_name;
          $post_gallery_image_thumb = $value->post_gallery_image_thumb;

          $gallery .= ' <li>
                              <div class="portfolio-thumb">
                                  <div class="gc_filter_cont_overlay_wrapper port_uper_div">
                                      <img src="'.$post_gallery_image_name.'" class="zoom image img-responsive" alt="service_img" />
                                      <div class="gc_filter_cont_overlay zoom_popup">
                                          <div class="gc_filter_text"><a href="'.$post_gallery_image_name.'"><i class="fa fa-plus"></i></a>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </li>';
        }
      }

    }
  }

// var_dump($blog_category_id);die();

  // blog services 

  ?>




<!-- Content -->
<div class="page-content">
   <!-- inner page banner -->
   <div class="dez-bnr-inr overlay-black-middle" style="background-image:url(<?php echo base_url().'assets/themes/theme/'?>images/banner/bnr1.jpg);">
      <div class="container">
         <div class="dez-bnr-inr-entry">
            <h1 class="text-white"><?php echo $post_title?></h1>
         </div>
      </div>
   </div>
   <!-- inner page banner END -->
   <!-- Breadcrumb row -->
   <div class="breadcrumb-row">
      <div class="container">
         <ul class="list-inline">
            <li><a href="<?php echo site_url().'home'?>">Home</a></li>
            <li><?php echo $post_title?></li>
         </ul>
      </div>
   </div>
   <!-- Breadcrumb row END -->
   <div class="content-area">
      <!-- Left & right section start -->
      <div class="container">
         <h2 class="post-title"><a href="#"><?php echo $post_title?></a></h2>
         <div class="row">
            <div class="col-md-12 ">
                 <div id="masonry" class="dez-blog-grid-3" style="position: relative; height: 402.983px;">
                 <?php
                  // var_dump($blog_category_id);die();
                    $blog_services_rs = $this->site_model->get_active_post_content_by_category($blog_category_id);

                    $services_result = '';
                    $number = 0;
                    if($blog_services_rs->num_rows() > 0)
                    {
                      foreach ($blog_services_rs->result() as $key => $value) {
                        # code...

                         $about_title = $value->post_title;
                        $post_id = $value->post_id;
                        $blog_category_name = $value->blog_category_name;
                        $blog_category_id = $value->blog_category_id;
                        $post_title = $value->post_title;
                        $web_name = $this->site_model->create_web_name($post_title);
                        $post_status = $value->post_status;
                        $post_views = $value->post_views;
                        $image_about = base_url().'assets/images/posts/'.$value->post_image;
                        $created_by = $value->created_by;
                        $modified_by = $value->modified_by;
                        $post_target = $value->post_target;
                        $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
                        $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
                        $description = strip_tags($value->post_content);
                        $mini_desc = strip_tags(implode(' ', array_slice(explode(' ', $description), 0, 25)));
                        $created = $value->created;
                        $day = date('j',strtotime($created));
                        $month = date('M',strtotime($created));
                        $year = date('Y',strtotime($created));
                        $created_on = date('jS M Y',strtotime($value->created));

                        if($number % 3 == 0)  {


                        }

                        $services_result .= '
                                            
                                             <div class="post card-container col-md-4" >
                                               <div class="blog-post blog-grid date-style-2">
                                                  <div class="dez-post-media dez-img-effect zoom-slow"> <a href="'.site_url().'view-service/'.$web_name.'"><img src="'.$image_about.'" alt=""></a> </div>
                                                  <div class="dez-post-info">
                                                     <div class="dez-post-title ">
                                                        <h3 class="post-title"><a href="'.site_url().'view-service/'.$web_name.'">'.$post_title.'</a></h3>
                                                     </div>
                                                     <div class="dez-post-text">
                                                        <p>'.$mini_desc.'</p>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>
                                          ';

                          if($number % 3 == 0)  {


                          }
                          $number++;
                      }
                    }

                      ?>


                  <?php echo $services_result?>
               </div>
               
            
            </div>
         </div>
         <!-- Left & right section  END -->
      </div>
  </div>
</div>


 <?php echo $this->load->view("site/our_partners", '');?>  