 <div class="team_wrapper med_toppadder40">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 col-lg-offset-2">
                <div class="team_heading_wrapper med_bottompadder40 wow fadeInDown" data-wow-delay="0.5s">
                    <h1 class="med_bottompadder20">MEET OUR SPECIALISTS</h1>
                    <img src="<?php echo base_url().'assets/themes/deepmind/'?>images/Icon_team.png" alt="line" class="med_bottompadder20">
                    <p>Our team is comprised of a dedicated, professional team that is ready to provide the service that you need.</p>
                </div>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="team_slider_wrapper">
                    <div class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="row">
                                <?php
                                $specialist_query = $this->site_model->get_active_items('Our Team');
                                  $specialist_item = '';
                                  if($specialist_query->num_rows() > 0)
                                  {
                                    $x=0;
                                    foreach($specialist_query->result() as $row)
                                    {
                                      $about_title = $row->post_title;
                                      $post_id = $row->post_id;
                                      $blog_category_name = $row->blog_category_name;
                                      $blog_category_id = $row->blog_category_id;
                                      $post_title = $row->post_title;
                                      $web_name = $this->site_model->create_web_name($post_title);
                                      $post_status = $row->post_status;
                                      $post_views = $row->post_views;
                                      $image_specialist = base_url().'assets/images/posts/'.$row->post_image;
                                      $created_by = $row->created_by;
                                      $modified_by = $row->modified_by;
                                      $post_target = $row->post_target;
                                      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
                                      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
                                      $description = strtoupper($row->post_content);
                                      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
                                      $created = $row->created;
                                      $day = date('j',strtotime($created));
                                      $month = date('M',strtotime($created));
                                      $year = date('Y',strtotime($created));
                                      $created_on = date('jS M Y',strtotime($row->created));
                                      $x++;
                                      if($x < 9)
                                      {
                                        $x = '0'.$x;
                                      }
                            
                                      
                                      ?>
                                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                            <div class="team_about">
                                                
                                                <div class="team_img">
                                                    <img src="<?php echo $image_specialist;?>" alt="img" class="img-responsive">
                                                </div>
                                                <div class="team_txt">
                                                    <h1><a href="#"><?php echo $post_title;?></a></h1>
                                                    <p>(<?php echo $post_target;?>)</p>
                                                </div>
                                                <div class="team_icon_hover our_doc_icn_hovr">
                                                    
                                                    <ul>
                                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                                    </ul>
                                                    <?php echo $mini_desc;?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                }

                            }
                                    ?>
                               
                                   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>