<?php
$about_query = $this->site_model->get_active_items('Company Testimonials');
 $testimonials_list = '';

 // var_dump($about_query);die();
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $about_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $post_title = $row->post_title;
    $web_name = $this->site_model->create_web_name($post_title);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $image_about = base_url().'assets/images/posts/'.$row->post_image;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $post_target = $row->post_target;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $description = $row->post_content;
    $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));
    $x++;
    if($x < 9)
    {
      $x = '0'.$x;
    }
    $testimonials_list .= '
                             <div class="item">
                                <div class="t_icon_wrapper">
                                    <div class="t_client_cont_wrapper2">
                                        <p>“ '.$description.' ”</p>
                                        <div class="client_img_abt">
                                            <img class="img-circle" src="'.$image_about.'" alt="img" style="width:90px;height:90px;">
                                            <h5>- '.$post_title.'</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>';
  }
}
?>
 <div class="testimonial_wrapper med_toppadder100">
    <div class="test_overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="test_heading_wrapper">
                    <h1 class="med_bottompadder20">What Patients Are Saying</h1>
                    <img src="<?php echo base_url().'assets/themes/deepmind/'?>images/line.png" alt="title" class="med_bottompadder60">
                </div>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="test_slider_wrapper">
                    <div class="owl-carousel owl-theme">
                       <?php echo $testimonials_list?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>