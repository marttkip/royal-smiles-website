<?php
        $specialist_query = $this->site_model->get_active_items('Why Choose Us');
          $main_services = '';
          if($specialist_query->num_rows() > 0)
          {
            $x=0;
            foreach($specialist_query->result() as $row)
            {
              $about_title = $row->post_title;
              $post_id = $row->post_id;
              $blog_category_name = $row->blog_category_name;
              $blog_category_id = $row->blog_category_id;
              $post_title = $row->post_title;
              $web_name = $this->site_model->create_web_name($post_title);
              $post_status = $row->post_status;
              $post_views = $row->post_views;
              $image_specialist = base_url().'assets/images/posts/'.$row->post_image;
              $created_by = $row->created_by;
              $modified_by = $row->modified_by;
              $post_target = $row->post_target;
              $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
              $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
              $description = $row->post_content;
              $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
              $created = $row->created;
              $day = date('j',strtotime($created));
              $month = date('M',strtotime($created));
              $year = date('Y',strtotime($created));
              $created_on = date('jS M Y',strtotime($row->created));
              $x++;
              if($x < 9)
              {
                $x = '0'.$x;
              }

              if($x== 1)
              {
              	$expandable="true";
                $show = 'show';
              }
              else
              {
              	$expandable = 'false';
                 $show = '';
              }
              $main_services .= '
              						<div class="panel panel-default sidebar_pannel">
	                                    <div class="panel-heading desktop">
	                                        <h4 class="panel-title">
	                                            <a data-toggle="collapse" data-parent="#accordionFifteenLeft" href="#collapseFifteenLeftone'.$post_id.'" aria-expanded="false">'.$about_title.'</a>
	                                        </h4>
	                                    </div>
	                                    <div id="collapseFifteenLeftone'.$post_id.'" class="panel-collapse collapse '.$show.'" aria-expanded="'.$expandable.'" role="tabpanel">
	                                        <div class="panel-body">
	                                            <div class="panel_cont">
	                                                <p>'.$mini_desc.'</p>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>';
        }
       }
        // $main_services = '</ul>';

      $gallery_rs =  $this->site_model->get_gallery('Company Gallery');
      $gallery_result = '';
      if($gallery_rs->num_rows() > 0)
      {
        foreach ($gallery_rs->result() as $key => $value) {
          # code...
          $gallery_name = $value->gallery_name;
          $gallery_image_name = $value->gallery_image_name;

          $image_specialist = base_url().'assets/gallery/'.$gallery_image_name;
          $gallery_result .= '<div class="item">
                                    <img src="'.$image_specialist.'" class="img-responsive" alt="story_img" />
                                </div>';
        }
      }
     ?>






 <div class="choose_wrapper med_toppadder100">
        <div class="choose_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="choose_heading_wrapper">
                        <h1 class="med_bottompadder20">Why Choose Us</h1>
                        <img src="images/line.png" alt="title" class="med_bottompadder30">
                    </div>
                    <div class="sidebar_wrapper">
                        <div class="accordionFifteen">
                            <div class="panel-group" id="accordionFifteenLeft" role="tablist">
                                <div class="panel panel-default sidebar_pannel">
                                    <div class="panel-heading desktop">
                                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordionFifteenLeft" href="index.html#collapseFifteenLeftone" aria-expanded="false">- We are always carefull to our patient  and service</a>
                        </h4>
                                    </div>
                                    <div id="collapseFifteenLeftone" class="panel-collapse collapse show" aria-expanded="true" role="tabpanel">
                                        <div class="panel-body">
                                            <div class="panel_cont">
                                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, aks lorem quis bibendum auctor, nisi elit c nibh vulputate..</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel-default -->
                                <div class="panel panel-default sidebar_pannel">
                                    <div class="panel-heading horn">
                                        <h4 class="panel-title">
                          <a class="collapsed" data-toggle="collapse" data-parent="#accordionFifteenLeft" href="index.html#collapseFifteenLeftTwo" aria-expanded="false">- Who has access to my Health Records</a>
                        </h4>
                                    </div>
                                    <div id="collapseFifteenLeftTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;" role="tabpanel">
                                        <div class="panel-body">
                                            <div class="panel_cont">
                                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, aks lorem quis bibendum auctor, nisi elit c nibh vulputate..</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel-default -->
                                <div class="panel panel-default sidebar_pannel">
                                    <div class="panel-heading bell">
                                        <h4 class="panel-title">
                          <a class="collapsed" data-toggle="collapse" data-parent="#accordionFifteenLeft" href="index.html#collapseFifteenLeftThree" aria-expanded="false">- We are always carefull to our patient  and service</a>
                        </h4>
                                    </div>
                                    <div id="collapseFifteenLeftThree" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;" role="tabpanel">
                                        <div class="panel-body">
                                            <div class="panel_cont">
                                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, aks lorem quis bibendum auctor, nisi elit c nibh vulputate..</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel-default -->
                                <div class="panel panel-default sidebar_pannel">
                                    <div class="panel-heading bell">
                                        <h4 class="panel-title">
                          <a class="collapsed" data-toggle="collapse" data-parent="#accordionFifteenLeft" href="index.html#collapseFifteenLeftFour" aria-expanded="false">- We are always carefull to our patient  and service</a>
                        </h4>
                                    </div>
                                    <div id="collapseFifteenLeftFour" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;" role="tabpanel">
                                        <div class="panel-body">
                                            <div class="panel_cont">
                                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, aks lorem quis bibendum auctor, nisi elit c nibh vulputate..</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel-default -->
                                <div class="panel panel-default sidebar_pannel">
                                    <div class="panel-heading bell">
                                        <h4 class="panel-title">
                          <a class="collapsed" data-toggle="collapse" data-parent="#accordionFifteenLeft" href="index.html#collapseFifteenLeftfive" aria-expanded="false">- We are always carefull to our patient  and service</a>
                        </h4>
                                    </div>
                                    <div id="collapseFifteenLeftfive" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;" role="tabpanel">
                                        <div class="panel-body">
                                            <div class="panel_cont">
                                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, aks lorem quis bibendum auctor, nisi elit c nibh vulputate..</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel-default -->
                                <div class="panel panel-default sidebar_pannel">
                                    <div class="panel-heading bell">
                                        <h4 class="panel-title">
                          <a class="collapsed" data-toggle="collapse" data-parent="#accordionFifteenLeft" href="index.html#collapseFifteenLeftsix" aria-expanded="false">- We are always carefull to our patient  and service</a>
                        </h4>
                                    </div>
                                    <div id="collapseFifteenLeftsix" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;" role="tabpanel">
                                        <div class="panel-body">
                                            <div class="panel_cont">
                                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, aks lorem quis bibendum auctor, nisi elit c nibh vulputate..</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel-default -->
                            </div>
                            <!--end of /.panel-group-->
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="owl_box">
                        <div class="med_slider_img">
                            <div class="owl-carousel owl-theme">
                                <?php echo $gallery_result?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>