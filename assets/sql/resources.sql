DROP TABLE IF EXISTS `resources`;
CREATE TABLE `resources`  (
	`resource_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`resource_title` text,
	`resource_name` text,
	`resource_status` boolean,
	`date_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
	`created_by` text,
	`modified_by` text
);